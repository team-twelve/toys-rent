from django.urls import path

from . import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.signup, name='signup'),
    path('tambah-item/', views.tambah_item, name='tambah-item'),
    path('update-item/', views.update_item, name='update-item'),
    path('daftar-item/', views.daftar_item, name='daftar-item'),
    path('delete-item/', views.delete_item, name='delete-item'),
    path('profil/', views.profil, name='profil'),
    path('chat/', views.chat, name='chat'),
    path('add-goods/', views.add_goods, name='add-goods'),
    path('add-goods/<str:nama_item>/', views.add_goods_slug, name='add-goods-slug'),
    path('update-goods/<str:id_barang>', views.update_goods, name='update-goods'),
    path('view-goods/', views.view_goods, name='view-goods'),
    path('view-goods/<str:nama_item>/', views.view_goods_slug, name='view-goods-slug'),
    path('view-goods-details/<str:id_barang>', views.view_goods_details, name='view-goods-details'),
    path('delete-goods/<str:id_barang>', views.delete_goods, name='delete-goods'),
    path('view-form-pesanan/', views.form_pemesanan, name='view-form-pesanan'),
    path('view-pesanan/', views.view_pemesanan, name='view-pesanan'),
    path('update-pesanan/<pesanan_slug>', views.update_pemesanan, name='update-pesanan'),
    path('view-level/', views.view_level, name='view-level'),
    path('form-level/', views.form_level, name='form-level'),
    path('update-level/<level_slug>', views.update_level, name='update-level'),
    path('view-review/', views.view_review, name='view-review'),
    path('view-pengiriman/', views.view_pengiriman, name='view-pengiriman'),
    path('form-review/', views.form_review, name='form-review'),
    path('update-review/', views.update_review, name='update-review'),
    path('view-form-pengiriman/', views.form_pengiriman, name='view-form-pengiriman'),
    path('get-item-info/', views.get_item_info, name='get-item-info'),
]
