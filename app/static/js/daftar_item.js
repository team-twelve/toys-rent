function init_button(){
	$('.update-item').click(function(e) {
    	e.preventDefault();
    	update_button_action($(this).val());
	});

	$('.delete-item').click(function(e) {
    	e.preventDefault();
    	show_delete_modal($(this).val());
	});

	$('#confirm-delete').click(function(e) {
    	e.preventDefault();
		  delete_button_action($(this).val());
	});

	$('.view-goods').click(function(e) {
      e.preventDefault();
      window.location.replace("/view-goods/" + $(this).val() + '/');
	});

  $('.add-goods').click(function(e) {
      e.preventDefault();
      window.location.replace("/add-goods/" + $(this).val() + '/');
  });
}

$(document).ready(function(){
	init_button()
	$('#daftar-item-table').DataTable();
});

function update_button_action(value) {
    const nama_item_data = {
      'csrfmiddlewaretoken': $('meta[name="csrf-token"]').attr('content'),
      'nama-item': value
    };
    $.ajax({
      url: "/get-item-info/",
      data: nama_item_data,
      type: "POST",
      success: function (result) {
      	sessionStorage['nama'] = result.nama;
      	sessionStorage['deskripsi'] = result.deskripsi;
      	sessionStorage['usia_minimal'] = result.usia_minimal;
      	sessionStorage['usia_maksimal'] = result.usia_maksimal;
      	sessionStorage['bahan'] = result.bahan;
      	sessionStorage['kategori'] = result.kategori;
		window.location.replace("/update-item");      	
      }
    });
}

function delete_button_action(value) {
    const nama_item_data = {
      'csrfmiddlewaretoken': $('meta[name="csrf-token"]').attr('content'),
      'nama-item': value
    };
    $.ajax({
      url: "/delete-item/",
      data: nama_item_data,
      type: "POST",
      success: function (result) {
		window.location.reload();      	
      }
    });
}

function show_delete_modal(value) {
	$('#modal-delete-body').text("Anda yakin ingin menghapus item " + value + "?")
	$('#modal-delete').modal('show');
	$('#confirm-delete').val(value);
}