$(document).ready(function(){
  fillForm();
  updateItem();
});

function fillForm() {
  $("#nama-item").val(sessionStorage['nama']);
  $("#deskripsi-item").val(sessionStorage['deskripsi']);
  $("#usia-minimal").val(sessionStorage['usia_minimal']);
  $("#usia-maksimal").val(sessionStorage['usia_maksimal']);
  $("#bahan-item").val(sessionStorage['bahan']);
  $("#kategori-item").val(sessionStorage['kategori']);
}

function updateItem() {
  $("#update-item-form").submit(function (e) {
    e.preventDefault();
    const updateItemData = {
      'csrfmiddlewaretoken': $('meta[name="csrf-token"]').attr('content'),
      'nama-item': $("#nama-item").val(),
      'deskripsi-item': $("#deskripsi-item").val(),
      'usia-minimal': $("#usia-minimal").val(),
      'usia-maksimal': $("#usia-maksimal").val(),
      'bahan-item': $("#bahan-item").val(),
      'kategori-item': $("#kategori-item").val(),
      'old-kategori-item': sessionStorage['kategori'].split(','),
      'old-nama-item': sessionStorage['nama'],    
    };
    $.ajax({
      url: "/update-item/",
      data: updateItemData,
      type: "POST",
      success: function (result) {
        if (result.success){
          window.location.replace("/daftar-item");
        } else {
          $("#error-text").text(result.error);
        }
      },
      error: function (result) {
        $("#error-text").text(result.error);
      }
    });
  });
}