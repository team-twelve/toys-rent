var alamat_counter = 1;

function change_form(){

	$('#anggota-button').click(function() {
		$('#anggota-button').removeClass('btn-secondary').addClass('btn-info');
		$('#admin-button').removeClass('btn-info').addClass('btn-secondary');
		$('#form-alamat').css('display', 'flex');
		$('#tipe_pengguna').attr('value', 'Anggota');
		$('.nama_alamat').attr('required', true);
		$('.alamat_jalan').attr('required', true);
		$('.alamat_nomor').attr('required', true);
		$('.alamat_kota').attr('required', true);
		$('.alamat_kode_pos').attr('required', true);
	});

	$('#admin-button').click(function() {
		$('#anggota-button').removeClass('btn-info').addClass('btn-secondary');
		$('#admin-button').removeClass('btn-secondary').addClass('btn-info');
		$('#form-alamat').css('display', 'none');
		$('#tipe_pengguna').attr('value', 'Admin');
		$('.nama_alamat').prop('required', false);
		$('.alamat_jalan').prop('required', false);
		$('.alamat_nomor').prop('required', false);
		$('.alamat_kota').prop('required', false);
		$('.alamat_kode_pos').prop('required', false);

	});
}


function modify_alamat() {
	$('#add-alamat').click(function() {
		alamat_counter += 1
		var $alamat = $('#dummy-alamat').clone()
		var inputs = $alamat.find('input');
		$.each(inputs, function(index, elem){
            var element = $(elem); 
            var name = element.prop('name');
            name += alamat_counter;
            element.prop('name', name);
        });
        $alamat.attr('id', 'form_alamat_'+alamat_counter);
		$alamat.removeClass("d-none").appendTo('#form-alamat');
	});

	$("#form-alamat").delegate(".delete-alamat", "click", function() {
		alamat_counter -= 1
		$(this).parent().remove();
	});
}

$(document).ready(function(){
	$('#tipe_pengguna').attr('value', 'Anggota');
	change_form();
	modify_alamat();
});




