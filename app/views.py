import uuid, random

from django.db import connection
from django.http import HttpResponse
from django.shortcuts import redirect, render, reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import DatabaseError, transaction
from django.http import JsonResponse
from datetime import datetime, timedelta
from .models import *


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def user_is_admin(no_ktp):
    with connection.cursor() as cursor:
        cursor.execute('SELECT no_ktp FROM admin WHERE no_ktp = \'{}\''.format(no_ktp))
        row = cursor.fetchone()
        if (row):
            return True
        return False

def is_authenticated(request):
    if 'Authorization' in request.session:
        with connection.cursor() as cursor:
            cursor.execute('SELECT no_ktp FROM pengguna WHERE no_ktp = %s', [request.session['Authorization']])
            row = cursor.fetchone()
            if (row):
                return True
    return False


def get_no_ktp(request):
    if 'Authorization' in request.session:
        return request.session['Authorization']
    else:
        return ''

def index(request):
    context = {}
    if (is_authenticated(request)):
        context['no_ktp'] = get_no_ktp(request)
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM status')
            rows = dictfetchall(cursor)
            context['statuses'] = rows
    return render(request, 'app/index.html', context=context)

def signup(request):
    response = {}
    if (request.method == 'POST'):
        tipe_pengguna = request.POST['tipe_pengguna']
        no_ktp = request.POST['no_ktp']
        nama = request.POST['nama']
        email = request.POST['email']
        tanggal_lahir = request.POST['tanggal_lahir'] if request.POST['tanggal_lahir'] != '' else None
        nomor_telepon = request.POST['nomor_telepon'] if request.POST['nomor_telepon'] != '' else None  

        try:
            list_of_alamat_post_key = list(request.POST)[7::]
        except:
            response['error'] = "Maaf, data yang Anda masukkan tidak sesuai"
            return render(request, 'app/signup.html', context=response)

        list_of_alamat = []
        alamat = []
        for i in range(len(list_of_alamat_post_key)):
            if (i%5==0 and i!=0):
                list_of_alamat.append(alamat)
                alamat=[]
            alamat.append(list_of_alamat_post_key[i])

        try: 
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO pengguna VALUES (%s, %s, %s, %s, %s)',\
                        [no_ktp, nama, email, tanggal_lahir, nomor_telepon])

                    if (tipe_pengguna == 'Anggota'):
                        cursor.execute('INSERT INTO anggota VALUES (%s, %s, %s)',\
                            [no_ktp, '0', 'Bronze'])
                        
                        for alamat in list_of_alamat:
                            cursor.execute('INSERT INTO alamat VALUES (%s, %s, %s, %s, %s, %s)',\
                                [no_ktp, \
                                request.POST[alamat[0]], \
                                request.POST[alamat[1]], \
                                request.POST[alamat[2]], \
                                request.POST[alamat[3]], \
                                request.POST[alamat[4]]])
                        
                        manage_login_session(request, tipe_pengguna)
                        return redirect(reverse('app:view-goods'))

                    if (tipe_pengguna == 'Admin'):
                        cursor.execute('INSERT INTO admin VALUES (%s)',\
                            [no_ktp])

                        manage_login_session(request, tipe_pengguna)
                        return redirect(reverse('app:view-pesanan'))

        except DatabaseError as e:
            response['error'] = "Maaf, nomor ktp / email Anda sudah terdaftar,\
             atau terdapat beberapa data yang tidak sesuai" + str(e)
            return render(request, 'app/signup.html', context=response)
    else:
        return render(request, 'app/signup.html')

def manage_login_session(request, role):
    request.session['Authorization'] = request.POST['no_ktp']
    if (role == "Anggota"):
        request.session['is_anggota'] = True
        request.session['is_admin'] = False
    if (role == "Admin"):
        request.session['is_admin'] = True
        request.session['is_anggota'] = False
        
def login(request):
    response = {}
    if (request.method == 'POST'):
        no_ktp = request.POST['no_ktp']
        with connection.cursor() as cursor:
            cursor.execute('SELECT email FROM pengguna WHERE no_ktp = %s', [no_ktp])
            row = cursor.fetchone()
            if (row and row[0] != '' and (request.POST['email'] == row[0])):
                manage_login_session(request, check_user_role(no_ktp))
                return redirect(reverse('app:index'))
            else:
                response['error'] = "Maaf, nomor ktp atau email Anda salah"
                return render(request, 'app/login.html', context=response)
    else:
        return render(request, 'app/login.html')

def check_user_role(no_ktp):
    with connection.cursor() as cursor:
        cursor.execute('SELECT no_ktp FROM admin WHERE no_ktp = %s', [no_ktp])
        admin = cursor.fetchone()
        if (admin):
            return "Admin"
        cursor.execute('SELECT no_ktp FROM anggota WHERE no_ktp = %s', [no_ktp])
        anggota = cursor.fetchone()
        if (anggota):
            return "Anggota"

def logout(request):
    request.session.flush()
    return redirect(reverse('app:index'))

def get_kategori(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT nama from kategori')
        row = cursor.fetchall()
        kategori = [elem[0] for elem in row] if (row) else []
    return kategori

def tambah_item(request):
    response = {}
    if (request.method == 'POST' \
        and check_user_role(request.session['Authorization']) == 'Admin') :
        nama = request.POST['nama-item']
        deskripsi = request.POST['deskripsi-item'] if request.POST['deskripsi-item']!='' else None 
        usia_minimal = request.POST['usia-minimal']
        usia_maksimal = request.POST['usia-maksimal']
        bahan = request.POST['bahan-item'] if request.POST['bahan-item']!='' else None 
        kategori = request.POST.getlist('kategori-item')
        try: 
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO item VALUES (%s, %s, %s, %s, %s)',\
                        [nama, deskripsi, usia_minimal, usia_maksimal, bahan])
                    if (kategori):
                        for i in kategori:
                            cursor.execute('INSERT INTO kategori_item VALUES (%s, %s)',\
                                [nama, i])

                    return redirect(reverse('app:daftar-item'))

        except DatabaseError as e:
            response['kategori'] = get_kategori(request)
            response['error'] = "Gagal menambahkan item, nama item sudah ada pada sistem "\
                + "atau terdapat data yang tidak sesuai"
            return render(request, 'app/tambah_item.html', context=response)
    else:
        response['kategori'] = get_kategori(request)
        return render(request, 'app/tambah_item.html', context=response)

def update_item(request):
    response = {}
    if (request.method == 'POST' \
        and check_user_role(request.session['Authorization']) == 'Admin') :
        nama = request.POST['nama-item']
        deskripsi = request.POST['deskripsi-item'] if request.POST['deskripsi-item']!='' else None 
        usia_minimal = request.POST['usia-minimal']
        usia_maksimal = request.POST['usia-maksimal']
        bahan = request.POST['bahan-item'] if request.POST['bahan-item']!='' else None
        kategori = request.POST.getlist('kategori-item[]')
        old_nama = request.POST['old-nama-item']  

        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE item SET ' \
                        + 'nama = %s, ' \
                        + 'deskripsi = %s, ' \
                        + 'usia_dari = %s, ' \
                        + 'usia_sampai = %s, ' \
                        + 'bahan = %s ' \
                        + 'WHERE nama = %s', \
                        [nama, deskripsi, usia_minimal, usia_maksimal, bahan, old_nama])

                    cursor.execute('DELETE FROM kategori_item ' \
                        + 'WHERE nama_item = %s ',
                        [old_nama])

                    if (kategori):
                        for i in kategori:
                            cursor.execute('INSERT INTO kategori_item VALUES (%s, %s)',\
                                [nama, i])

                    response['success'] = True
                    return JsonResponse(response)
        except DatabaseError as e:
            response['kategori'] = get_kategori(request)
            response['success'] = False
            response['error'] = "Gagal update item, nama item setelah update sama dengan nama item lain pada sistem "\
                + "atau terdapat data yang tidak sesuai" + str(e)
            return JsonResponse(response)
    else:
        response['kategori'] = get_kategori(request)
        return render(request, 'app/update_item.html', context=response)

def get_item_info(request):
    response = {}
    if (request.method == 'POST') :
        nama = request.POST['nama-item']    
        with connection.cursor() as cursor:
            cursor.execute('SELECT * from item WHERE nama = %s', [nama])
            row = cursor.fetchone()
            if(row):
                response['nama'] = row[0]
                response['deskripsi'] = row[1] if row[1]!=None else '' 
                response['usia_minimal'] = row[2]
                response['usia_maksimal'] = row[3]
                response['bahan'] = row[4] if row[4]!=None else ''
            
            cursor.execute('SELECT nama_kategori from kategori_item WHERE nama_item = %s', [nama])
            row = cursor.fetchall()
            response['kategori'] = row if (row) else ''
            return JsonResponse(response)
    

def delete_item(request):
    response = {}
    if (request.method == 'POST' \
        and check_user_role(request.session['Authorization']) == 'Admin') :
        nama = request.POST['nama-item'] 
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('DELETE FROM item WHERE nama = %s', \
                        [nama])
                    response['success'] = True
                    return JsonResponse(response)
        except DatabaseError as e:
            response['success'] = False
            return JsonResponse(response)
    else:
        response['success'] = False
        return JsonResponse(response)      

def daftar_item(request):
    if 'Authorization' not in request.session:
        return redirect(reverse('app:login'))
    response = {}
    with connection.cursor() as cursor:
        cursor.execute('SELECT nama FROM item')
        item_row = cursor.fetchall()
        response['daftar_item'] = []
        for item_data in reversed(item_row):
            item = {}
            nama_item = item_data[0]
            item['nama_item'] = nama_item

            cursor.execute('SELECT nama_kategori from kategori_item WHERE nama_item = %s', [nama_item])
            kategori_item = cursor.fetchall()
            item['kategori_item'] = ''
            for i in range(len(kategori_item)):                    
                item['kategori_item'] += kategori_item[i][0]
                if i != len(kategori_item) - 1:
                    item['kategori_item'] += ', '

            response['daftar_item'].append(item)

    return render(request, 'app/daftar_item.html', context=response)

def profil(request):
    if 'Authorization' not in request.session:
        return redirect(reverse('app:login'))
    response = {}
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pengguna WHERE no_ktp = %s', [request.session['Authorization']])
        row = cursor.fetchone()
        if (row):
            response['no_ktp'] = row[0]
            response['nama'] = row[1]
            response['email'] = row[2]
            response['tanggal_lahir'] = row[3]
            response['nomor_telepon'] = row[4]

        cursor.execute('SELECT * FROM anggota WHERE no_ktp = %s', [request.session['Authorization']])
        row = cursor.fetchone()
        if (row):
            response['poin'] = row[1]
            response['level'] = row[2]

        cursor.execute('SELECT * FROM alamat WHERE no_ktp_anggota = %s', [request.session['Authorization']])
        row = cursor.fetchall()
        if (row):
            response['daftar_alamat'] = []
            for data in row:
                alamat = {}
                alamat['nama_alamat'] = data[1]
                alamat['alamat_lengkap'] = data[2] + " No. "\
                    + str(data[3]) + ", " + data[4]+ ". Kode Pos : "\
                    + data[5]
                response['daftar_alamat'].append(alamat)

    return render(request, 'app/profil.html', context=response)

def chat(request):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            if (context['user'] == 'admin' and check_user_role(request.POST['no-ktp']).lower() == 'anggota'):
                cursor.execute('INSERT INTO chat VALUES (%s, %s, %s, %s, %s)',\
                    [str(uuid.uuid4())[:13], request.POST['pesan'], datetime.now().strftime('%Y-%m-%d %H:%M:%S'), request.POST['no-ktp'], no_ktp])
            elif (context['user'] == 'pengguna' and check_user_role(request.POST['no-ktp']).lower() == 'admin'):
                cursor.execute('INSERT INTO chat VALUES (%s, %s, %s, %s, %s)',\
                    [str(uuid.uuid4())[:13], request.POST['pesan'], datetime.now().strftime('%Y-%m-%d %H:%M:%S'), no_ktp, request.POST['no-ktp']])
        return redirect(reverse('app:chat'))
    else:
        with connection.cursor() as cursor:
            if (context['user'] == 'admin'):
                cursor.execute('SELECT * FROM chat')
            else:
                cursor.execute('SELECT * FROM chat WHERE no_ktp_anggota = %s', [no_ktp])
            rows = dictfetchall(cursor)
            context['chats'] = rows
        return render(request, 'app/chat.html', context=context)


def add_goods(request):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            lama_penggunaan = request.POST['lama-penggunaan'] if request.POST['lama-penggunaan'] != '' else None
            cursor.execute('INSERT INTO barang VALUES (%s, %s, %s, %s, %s, %s, %s)',\
                [request.POST['id-barang'], request.POST['nama-item'], request.POST['warna'],
                request.POST['url-foto'], request.POST['kondisi'], lama_penggunaan,
                request.POST['pemilik-penyewa']]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Bronze', int(request.POST['harga-sewa-bronze']) / 100,
                int(request.POST['persen-royalty-bronze']) / 100]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Silver', int(request.POST['harga-sewa-silver']) / 100,
                int(request.POST['persen-royalty-silver']) / 100]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Gold', int(request.POST['harga-sewa-gold']) / 100,
                int(request.POST['persen-royalty-gold']) / 100]
            )
        return redirect(reverse('app:view-goods'))
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT nama FROM item")
            rows = dictfetchall(cursor)
            context['items'] = rows
            cursor.execute("SELECT no_ktp, nama_lengkap FROM pengguna NATURAL JOIN anggota")
            rows = dictfetchall(cursor)
            context['pemiliks_penyewas'] = rows
        return render(request, 'app/add-goods.html', context=context)

def add_goods_slug(request, nama_item):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            lama_penggunaan = request.POST['lama-penggunaan'] if request.POST['lama-penggunaan'] != '' else None
            cursor.execute('INSERT INTO barang VALUES (%s, %s, %s, %s, %s, %s, %s)',\
                [request.POST['id-barang'], request.POST['nama-item'], request.POST['warna'],
                request.POST['url-foto'], request.POST['kondisi'], lama_penggunaan,
                request.POST['pemilik-penyewa']]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Bronze', float(request.POST['harga-sewa-bronze']) / 100,
                float(request.POST['persen-royalty-bronze']) / 100]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Silver', float(request.POST['harga-sewa-silver']) / 100,
                float(request.POST['persen-royalty-silver']) / 100]
            )
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)",\
                [request.POST['id-barang'], 'Gold', float(request.POST['harga-sewa-gold']) / 100,
                float(request.POST['persen-royalty-gold']) / 100]
            )
        return redirect(reverse('app:view-goods'))
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT nama FROM item")
            rows = dictfetchall(cursor)
            context['items'] = rows
            cursor.execute("SELECT no_ktp, nama_lengkap FROM pengguna NATURAL JOIN anggota")
            rows = dictfetchall(cursor)
            context['pemiliks_penyewas'] = rows
        return render(request, 'app/add-goods.html', context=context)


def update_goods(request, id_barang):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            lama_penggunaan = request.POST['lama-penggunaan'] if request.POST['lama-penggunaan'] != '' else None
            cursor.execute('UPDATE barang SET nama_item = %s, warna = %s, url_foto = %s, kondisi = %s, lama_penggunaan = %s, no_ktp_penyewa = %s WHERE id_barang = %s',\
                [request.POST['nama-item'], request.POST['warna'], request.POST['url-foto'],
                request.POST['kondisi'], lama_penggunaan, request.POST['pemilik-penyewa'],
                request.POST['id-barang']]
            )
            cursor.execute("UPDATE info_barang_level SET harga_sewa = %s, porsi_royalti = %s WHERE id_barang = %s AND nama_level = 'Bronze'",\
                [float(request.POST['harga-sewa-bronze']), float(request.POST['persen-royalty-bronze']) / 100, request.POST['id-barang']]
            )
            cursor.execute("UPDATE info_barang_level SET harga_sewa = %s, porsi_royalti = %s WHERE id_barang = %s AND nama_level = 'Silver'",\
                [float(request.POST['harga-sewa-silver']), float(request.POST['persen-royalty-silver']) / 100, request.POST['id-barang']]
            )
            cursor.execute("UPDATE info_barang_level SET harga_sewa = %s, porsi_royalti = %s WHERE id_barang = %s AND nama_level = 'Gold'",\
                [float(request.POST['harga-sewa-gold']), float(request.POST['persen-royalty-gold']) / 100, request.POST['id-barang']])
        return redirect(reverse('app:view-goods'))
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT nama FROM item")
            rows = dictfetchall(cursor)
            context['items'] = rows
            cursor.execute("SELECT no_ktp, nama_lengkap FROM pengguna NATURAL JOIN anggota")
            rows = dictfetchall(cursor)
            context['pemiliks_penyewas'] = rows
            cursor.execute("\
                SELECT b.*, p.no_ktp AS no_ktp_penyewa, p.nama_lengkap AS pemilik_penyewa \
                FROM barang b, pengguna p, anggota a \
                WHERE b.no_ktp_penyewa = a.no_ktp AND a.no_ktp = p.no_ktp AND id_barang = %s",
                [id_barang])
            rows = dictfetchall(cursor)
            context['default'] = rows[0]
            cursor.execute("SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = 'Bronze'", [id_barang])
            rows = dictfetchall(cursor)
            context['default_bronze_info'] = rows[0]
            cursor.execute("SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = 'Silver'", [id_barang])
            rows = dictfetchall(cursor)
            context['default_silver_info'] = rows[0]
            cursor.execute("SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = 'Gold'", [id_barang])
            rows = dictfetchall(cursor)
            context['default_gold_info'] = rows[0]
        return render(request, 'app/update-goods.html', context=context)


def delete_goods(request, id_barang):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM barang WHERE id_barang = %s', [id_barang])
        return redirect(reverse('app:view-goods'))


def view_goods(request):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            if (request.POST['urutan'] == 'nama'):
                cursor.execute('SELECT * FROM barang ORDER BY nama_item')
            elif (request.POST['urutan'] == 'kategori'):
                cursor.execute('\
                    SELECT b.* \
                    FROM item i, barang b, kategori k, kategori_item ki \
                    WHERE i.nama = b.nama_item and ki.nama_item = i.nama and ki.nama_kategori = k.nama \
                    ORDER BY k.nama')
            rows = dictfetchall(cursor)
            context['goods'] = rows
        return render(request, 'app/view-goods.html', context=context)
    else:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM barang')
            rows = dictfetchall(cursor)
            context['goods'] = rows
        return render(request, 'app/view-goods.html', context=context)


def view_goods_slug(request, nama_item):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    if (request.method == 'POST'):
        with connection.cursor() as cursor:
            if (request.POST['urutan'] == 'nama'):
                cursor.execute('SELECT * FROM barang WHERE nama_item=%s ORDER BY nama_item',[nama_item])
            elif (request.POST['urutan'] == 'kategori'):
                cursor.execute('\
                    SELECT b.* \
                    FROM item i, barang b, kategori k, kategori_item ki \
                    WHERE i.nama = b.nama_item and ki.nama_item = i.nama and ki.nama_kategori = k.nama and nama_item=%s \
                    ORDER BY k.nama',[nama_item])
            rows = dictfetchall(cursor)
            context['goods'] = rows
        return render(request, 'app/view-goods.html', context=context)
    else:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM barang  WHERE nama_item=%s',[nama_item])
            rows = dictfetchall(cursor)
            context['goods'] = rows
        return render(request, 'app/view-goods.html', context=context)


def view_goods_details(request, id_barang):
    if (not is_authenticated(request)):
        return redirect(reverse('app:login'))
    context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    context['user'] = role
    with connection.cursor() as cursor:
        cursor.execute('\
            SELECT b.*, p.nama_lengkap AS pemilik_penyewa \
            FROM pengguna p, anggota a, barang b \
            WHERE p.no_ktp = a.no_ktp AND a.no_ktp = b.no_ktp_penyewa AND b.id_barang = %s', [id_barang])
        rows = dictfetchall(cursor)
        context['goods'] = rows[0]
        cursor.execute('SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = \'Bronze\'', [id_barang])
        rows = dictfetchall(cursor)
        context['bronze_info'] = rows[0]
        cursor.execute('SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = \'Silver\'', [id_barang])
        rows = dictfetchall(cursor)
        context['silver_info'] = rows[0]
        cursor.execute('SELECT ibl.*, ibl.porsi_royalti * 100 AS persen_royalti FROM info_barang_level ibl WHERE id_barang = %s AND nama_level = \'Gold\'', [id_barang])
        rows = dictfetchall(cursor)
        context['gold_info'] = rows[0]
        rows = dictfetchall(cursor)
    return render(request, 'app/view-goods-details.html', context=context)


def form_pemesanan(request):
    list_context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    if (is_authenticated(request)):
        if request.method == 'POST':
            user_confirm_buy = False
            if 'confirm' in request.POST.keys():
                user_confirm_buy = request.POST['confirm']
            print(user_confirm_buy)
            print('confirm' in request.POST.keys())
            if(user_confirm_buy):
                no_ktp = request.POST['no_ktp']
                barang = request.POST['barang']
                waktu_sewa = request.POST['waktu']
                harga = request.POST['harga']
                different_time = datetime.now() + timedelta(days = int(waktu_sewa))
                with connection.cursor() as cursor:
                    cursor.execute('select * from pemesanan')
                    generate_id = dictfetchall(cursor)
                    generate_id = len(generate_id) + 1
                    generate_id = 'id_pesan'+str(generate_id)
                    print(generate_id)
                    datetime_now = datetime.now()
                    cursor.execute('insert into pemesanan values(\'{}\', \'{}\', \'{}\', \'{}\' ,\'{}\' ,\'{}\', \'{}\')'.format(generate_id, datetime_now, len(barang),int(harga), 0, no_ktp, 'Kode terdaftar'))
                    cursor.execute("select max(cast(no_urut as int)) as no_urut from barang_pesanan")
                    no_urut = dictfetchall(cursor)[0].get("no_urut") + 1
                    no_urut = int(next_id.split('id_pesan')[1]) + 1
                    no_urut = str('id_pesan'+str(next_id))
                    for barang in barang:
                        cursor.execute('insert into barang_pesanan values(\'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\')'.format(generate_id, no_urut, barang, datetime_now, waktu_sewa, different_time, 'Kode terdaftar'))
                        no_urut = no_urut + 1
                    return redirect('/view-pemesanan')
            else:
                no_ktp = request.POST['anggota']
                barang = request.POST['barang']
                waktu = request.POST['waktu']
                list_context["anggota"] = no_ktp
                list_context["list_barang"] = barang
                list_context["waktu"] = waktu
                with connection.cursor() as cursor:
                    cursor.execute('select no_ktp from anggota where no_ktp = \'{}\''.format(no_ktp))
                    row = dictfetchall(cursor)
                    print("Pepeghe")
                    if row:
                        print("_e-ega")
                        total = 0
                        for barang in barang:
                            cursor.execute("select harga_sewa from info_barang_level, anggota where id_barang = '" + barang + "' and anggota.no_ktp = '" + no_ktp +"' and info_barang_level.nama_level = anggota.level;")
                            total = total + int(dictfetchall(cursor)[0]['harga_sewa'])
                        list_context['total'] = total
                    print(list_context['total'])
    if role == 'admin':
        with connection.cursor() as cursor:
            cursor.execute("select no_ktp from anggota");
            rows = dictfetchall(cursor)
            list_context['list_anggota'] = rows
    else:
        list_context['list_anggota'] = no_ktp
    with connection.cursor() as cursor:
        cursor.execute('select nama_item, id_barang from barang')
        rows = dictfetchall(cursor)
        list_context['barang'] = rows
    list_context['tipe_pengguna'] = role
                
    return render(request, 'app/form-pemesanan.html', context=list_context)
    
def view_pemesanan(request):
    list_context = {}
    if (is_authenticated(request)):
        if request.method == 'POST':
            no_ktp = get_no_ktp(request)
            try:
                id_pemesanan = request.POST['id_pemesanan']
                status_pemesanan = request.POST['status_pemesanan']
                print(id_pemesanan)
                print(status_pemesanan)
                if status_pemesanan == 'Kode terdaftar':
                    with connection.cursor() as cursor:
                        cursor.execute('select anggota.no_ktp from anggota, pemesanan where anggota.no_ktp = pemesanan.no_ktp_pemesan and anggota.no_ktp = \'{}\''.format(no_ktp))
                        row = cursor.fetchone()
                        print("Pepega")
                        if (row):
                            cursor.execute('delete from barang_pesanan where id_pemesanan = \'{}\''.format(id_pemesanan))
                            print("Pepega2")
                            cursor.execute('delete from pemesanan where pemesanan.id_pemesanan = \'{}\'and pemesanan.no_ktp_pemesan = \'{}\''.format(id_pemesanan,no_ktp))
                        else:
                            list_context['login_first'] = 'Login first before you delete something that not belong to you'
                        return render(request, 'app/view-pemesanan.html', context=list_context)
                list_context['err'] = 'Pesanan dengan status selain "kode terdaftar" tidak bisa melakukan penghapusan pemesanan'
                return render(request, 'app/view-pemesanan.html', context=list_context)
            except:
                list_context['err'] = 'Oops something bad happen'
                return render(request, 'app/view-pemesanan.html', context=list_context)
        no_ktp = get_no_ktp(request)
        role = check_user_role(no_ktp).lower()
        if role == 'admin':
            list_context['tipe_pengguna'] = 'Admin'
        else:
            list_context['tipe_pengguna'] = 'Anggota'
        with connection.cursor() as cursor:
            if list_context['tipe_pengguna'] == 'Admin':
                cursor.execute('select distinct P.id_pemesanan, P.kuantitas_barang, P.harga_sewa, S.deskripsi, B2.nama_item from barang as B2, pemesanan as P,status as S,barang_pesanan as B where P.status = S.nama and P.id_pemesanan = B.id_pemesanan and B2.id_barang = B.id_barang;')
            else:
                cursor.execute('select distinct P.id_pemesanan, P.kuantitas_barang, P.harga_sewa, S.deskripsi, B2.nama_item from barang as B2, pemesanan as P, status as S, barang_pesanan as B where P.status = S.nama and P.id_pemesanan = B.id_pemesanan and B2.id_barang = B.id_barang and P.no_ktp_pemesan = \'{}\''.format(no_ktp))
            rows = dictfetchall(cursor)
            list_context['pemesanan'] = rows
        return render(request, 'app/view-pemesanan.html', context=list_context)
    list_context['login_first'] = 'Login to see the lists'
    return render(request, 'app/view-pemesanan.html', context=list_context)
    

def update_pemesanan(request, pesanan_slug):
    list_context = {}
    no_ktp = get_no_ktp(request)
    role = check_user_role(no_ktp).lower()
    if (is_authenticated(request)):
        no_ktp = get_no_ktp(request)
        role = check_user_role(no_ktp).lower()
        if request.method == 'POST' and role == 'admin':
            status = request.POST['status']
            id_pemesanan = request.POST['id_pemesanan']
            waktu = request.POST['waktu']
            print(status, id_pemesanan, waktu)
            try:
                with connection.cursor() as cursor:
                    print("Pepega")
                    cursor.execute('update pemesanan set status = \'{}\' where id_pemesanan = \'{}\''.format(status, id_pemesanan))
                    print("Pepega 2")
                    cursor.execute('update barang_pesanan set status = \'{}\', lama_sewa = \'{}\' where id_pemesanan = \'{}\''.format(status,int(waktu),id_pemesanan))
            except:
                list_context['err'] = 'Something bad happen'
                return render(request, 'app/update-level.html', context= list_context)
            return redirect('/view-pemesanan')
        if request.method == 'POST' and role == 'anggota':
            id_pemesanan = request.POST['id_pemesanan']
            waktu = request.POST['waktu']
            try:
                with connection.cursor() as cursor:
                    cursor.execute('update barang_pesanan set lama_sewa = \'{}\' where id_pemesanan = \'{}\''.format(waktu,id_pemesanan))
            except:
                list_context['err'] = 'Something bad happen'
                return render(request, 'app/update-level.html', context= list_context)
            return redirect('/view-level')
    with connection.cursor() as cursor:
        cursor.execute('select B.nama_item, B2.lama_sewa, P.status from barang B, barang_pesanan B2, pemesanan P where P.id_pemesanan = \'{}\' and P.id_pemesanan = B2.id_pemesanan and B2.id_barang = B.id_barang'.format(pesanan_slug))
        rows = dictfetchall(cursor)
        if not rows:
            return redirect('/view-pemesanan')
        waktu_sewa = rows[0]['lama_sewa']
        for barang in rows:
            list_barang = barang['nama_item'] + ', '
        list_context['status'] = rows[0]['status']
        list_context['waktu'] = waktu_sewa
        list_context['list_barang'] = list_barang
    with connection.cursor() as cursor:
        cursor.execute('select nama from status')
        list_context['list_status'] = dictfetchall(cursor)
    if role == 'admin':
        list_context['tipe_pengguna'] = 'Admin'
    else:
        list_context['tipe_pengguna'] = 'Anggota'
    list_context['id_barang'] = pesanan_slug
    #TODO LIST
    #ALHAMDULILLAH KELAR
    return render(request, 'app/update-pesanan.html', context=list_context)
        
def update_level(request, level_slug):
    list_context = {}
    if (is_authenticated(request)):
        no_ktp = get_no_ktp(request)
        role = check_user_role(no_ktp).lower()
        if request.method == 'POST' and role == 'admin':
            nama_level = request.POST['nama_level']
            minimum_poin = request.POST['minimum_poin']
            deskripsi = request.POST['deskripsi']
            try:
                with connection.cursor() as cursor:
                    cursor.execute('update level_keanggotaan set nama_level = \'{}\', minimum_poin = \'{}\', deskripsi = \'{}\' where nama_level = \'{}\''.format(nama_level, minimum_poin, deskripsi, level_slug))
            except:
                list_context['err'] = 'Something bad happen'
                return render(request, 'app/update-level.html', context= list_context)
            return redirect('/view-level')
        if role == 'admin':
            list_context['tipe_pengguna'] = 'Admin'
            with connection.cursor() as cursor:
                cursor.execute('select * from level_keanggotaan where nama_level = \'{}\''.format(level_slug))
                rows = dictfetchall(cursor)
                if not rows:
                    return redirect('/view-level')
                list_context['nama_level'] = rows[0]['nama_level']
                list_context['minimum_poin'] = rows[0]['minimum_poin']
                list_context['deskripsi'] = rows[0]['deskripsi']
            return render(request, 'app/update-level.html', context= list_context)    
        list_context['tipe_pengguna'] = 'Anggota'
    return render(request, 'app/update-level.html', context= list_context)
    
def form_level(request):
    list_context = {}
    if (is_authenticated(request)):
        no_ktp = get_no_ktp(request)
        role = check_user_role(no_ktp).lower()
        if request.method == 'POST' and role == 'admin':
            nama_level = request.POST['nama_level']
            minimum_poin = request.POST['minimum_poin']
            deskripsi = request.POST['deskripsi']
            try:
                with connection.cursor() as cursor:
                    cursor.execute('insert into level_keanggotaan values (\'{}\', \'{}\', \'{}\''\
                            .format(nama_level, minimum_poin, deskripsi))
            except:
                list_context['err'] = 'Something bad happen'
                return render(request, 'app/form-level.html', context=list_context)
            return redirect('/view-level')
    return render(request, 'app/form-level.html', context=list_context)
        
def view_level(request):
    list_context = {}
    if (is_authenticated(request)):
        if request.method == 'POST':
            try:
                nama_level = request.POST['nama_level']
                print(nama_level)
                with connection.cursor() as cursor:
                    cursor.execute('select admin.no_ktp from admin where admin.no_ktp = \'{}\''.format(get_no_ktp(request)))
                    row = cursor.fetchone()
                    if (row):
                        cursor.execute('delete from level_keanggotaan where nama_level = \'{}\''.format(nama_level))
                    else:
                        list_context['login_first'] = 'Only admin can delete level, reload again'
                    return render(request, 'app/view-level.html', context=list_context)
            except:
                list_context['err'] = 'Oops something bad happen'
                return render(request, 'app/view-level.html', context=list_context)
        no_ktp = get_no_ktp(request)
        role = check_user_role(no_ktp).lower()
        if role == 'admin':
            list_context['tipe_pengguna'] = 'Admin'
        else:
            list_context['tipe_pengguna'] = 'Anggota'
        with connection.cursor() as cursor:
            cursor.execute('select * from level_keanggotaan;')
            rows = dictfetchall(cursor)
            list_context['levels'] = rows
        return render(request, 'app/view-level.html', context=list_context)
    list_context['login_first'] = 'Login to see the lists'
    return render(request, 'app/view-level.html', context=list_context)

def view_review(request):
    list_context = {
        'reviews' : [
            {
                'id_review': 1,
                'nama_barang': 'Botol',
                'tanggal': '10/11/2018',
                'deskripsi': 'bagus botolnya',
            },
            {
                'id_review': 2,
                'nama_barang': 'hp',
                'tanggal': '10/12/2019',
                'deskripsi': 'hp nya bagus',
            }
        ]
    }
    return render(request, 'app/view-review.html', context=list_context)

def view_pengiriman(request):
    list_context = {
        'user' : random.choice(['admin','anggota']),
        'pengirimans' : [
            {
                'no_resi': '123XW312',
                'nama_barang': 'laptop',
                'tanggal': '10/11/2018',
                'status': 'TERKIRIM',
            },
            {
                'no_resi': '123XW313',
                'nama_barang': 'komputer',
                'tanggal': '10/12/2019',
                'status': 'PERJALANAN',
            }
        ]
    }
    return render(request, 'app/view-pengiriman.html', context=list_context)

def form_review(request):
    return render(request, 'app/form-review.html')

def update_review(request):
    list_context = {
        'user' : random.choice(['anggota','pepega']),
        'reviews': [ 
            {
                'id_review': 1,
                'nama_barang': 'Botol',
                'tanggal': '10/11/2018',
                'deskripsi': 'bagus botolnya',
            }, 
            {
                'id_review': 2,
                'nama_barang': 'hp',
                'tanggal': '10/12/2019',
                'deskripsi': 'hp nya bagus',
            }
        ]
    }
    return render(request, 'app/update-review.html', context=list_context)

def form_pengiriman(request):
    list_context = {
        'user': random.choice(['admin','pleb']),
        'anggota': {
            'Pepega',
            'Pepege',
            'Pepeok',
        },
        'barang': {
            'Ultimate HP Potion',
            'Ultimate Stamina Potion',
            'Ultimate MP Potion',
        }        
    }
    return render(request, 'app/form-pengiriman.html', context=list_context)