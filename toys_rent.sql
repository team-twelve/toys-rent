--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toys_rent; Type: SCHEMA; Schema: -; Owner: db2018027
--

CREATE SCHEMA toys_rent;


ALTER SCHEMA toys_rent OWNER TO db2018027;

--
-- Name: set_kondisi_barang(); Type: FUNCTION; Schema: public; Owner: db2018027
--

CREATE FUNCTION public.set_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE barang SET kondisi = 'Sedang disewa anggota'
WHERE id_barang = NEW.id_barang;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION public.set_kondisi_barang() OWNER TO db2018027;

--
-- Name: update_harga_sewa_pemesanan(); Type: FUNCTION; Schema: public; Owner: db2018027
--

CREATE FUNCTION public.update_harga_sewa_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    temp_no_ktp_pemesan VARCHAR(20);
    temp_harga_sewa INTEGER;
    temp_level VARCHAR(20);

  BEGIN
    IF (TG_OP = 'INSERT') THEN
      SELECT p.no_ktp_pemesan INTO temp_no_ktp_pemesan
      FROM pemesanan AS p JOIN barang_pesanan AS bp ON p.id_pemesanan = bp.id_pemesanan
      WHERE p.id_pemesanan = NEW.id_pemesanan;

      SELECT a.level INTO temp_level
      FROM anggota AS a
      WHERE a.no_ktp = temp_no_ktp_pemesan;

      SELECT ibl.harga_sewa INTO temp_harga_sewa
      FROM info_barang_level AS ibl
      WHERE ibl.id_barang = NEW.id_barang AND ibl.nama_level = temp_level;

      UPDATE pemesanan
      SET harga_sewa = harga_sewa + temp_harga_sewa
      WHERE id_pemesanan = NEW.id_pemesanan;
    END IF;
  END;
$$;


ALTER FUNCTION public.update_harga_sewa_pemesanan() OWNER TO db2018027;

--
-- Name: update_poin(); Type: FUNCTION; Schema: public; Owner: db2018027
--

CREATE FUNCTION public.update_poin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    no_ktp varchar;
  BEGIN
    SELECT A.no_ktp INTO no_ktp
    FROM ANGGOTA AS A
    WHERE A.no_ktp = NEW.no_ktp_pemesan;
    IF (TG_OP = 'INSERT') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.Poin + NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
    IF (TG_OP = 'DELETE') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.poin - NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
    IF (TG_OP = 'UPDATE') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.poin - OLD.Kuantitas_barang * 100 + NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
  END
$$;


ALTER FUNCTION public.update_poin() OWNER TO db2018027;

--
-- Name: cek_update_level(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.cek_update_level() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE 
  temp_row record;
  new_level varchar(20);
 BEGIN

  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
   -- Set base level to null, there may be 'anggota' with 'poin'
   -- below minimum 'poin' for minimum 'level_keanggotaan'
   new_level := NULL;
   -- Search for appropriate level
   FOR temp_row IN
    SELECT LK.nama_level AS nama_level, 
    LK.minimum_poin AS minimum_poin
    FROM level_keanggotaan LK
    ORDER BY LK.minimum_poin
   LOOP
    IF (NEW.poin >= temp_row.minimum_poin) THEN
     new_level := temp_row.nama_level;
    END IF;
   END LOOP;
   -- Update level
   UPDATE ANGGOTA A
   SET level = new_level
   WHERE A.no_ktp = NEW.no_ktp;
   
   RETURN NEW;
  END IF;
 
 END;
$$;


ALTER FUNCTION toys_rent.cek_update_level() OWNER TO db2018027;

--
-- Name: cek_update_level_semua_anggota(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.cek_update_level_semua_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE 
  min_poin_for_min_level real;
  temp_row record;
 BEGIN
   -- Search minimum 'poin' for minimum 'level_keanggotaan'
   SELECT LK.minimum_poin INTO min_poin_for_min_level
   FROM level_keanggotaan LK
   ORDER BY LK.minimum_poin LIMIT 1;
   
   -- Set 'level' to null for all 'anggota' with 'poin' below
   -- minimum 'poin' for minimum 'level_keanggotaan'
   UPDATE ANGGOTA A
   SET level = NULL
   WHERE A.poin < min_poin_for_min_level;

   -- Update level for each anggota
   FOR temp_row IN
    SELECT LK.nama_level AS nama_level, 
    LK.minimum_poin AS minimum_poin
    FROM level_keanggotaan LK
    ORDER BY LK.minimum_poin
   LOOP
    UPDATE ANGGOTA A
    SET level = temp_row.nama_level
    WHERE A.poin >= temp_row.minimum_poin;
   END LOOP;
  
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
   RETURN NEW;
  END IF;
  IF (TG_OP = 'DELETE') THEN
   RETURN OLD;
  END IF;

 END;
$$;


ALTER FUNCTION toys_rent.cek_update_level_semua_anggota() OWNER TO db2018027;

--
-- Name: set_kondisi_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.set_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE barang SET kondisi = 'Sedang disewa anggota'
WHERE id_barang = NEW.id_barang;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION toys_rent.set_kondisi_barang() OWNER TO db2018027;

--
-- Name: set_kondisi_barang_pengembalian(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.set_kondisi_barang_pengembalian() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE barang SET kondisi = 'Barang siap dipesan'
WHERE id_barang = NEW.id_barang;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION toys_rent.set_kondisi_barang_pengembalian() OWNER TO db2018027;

--
-- Name: update_harga_sewa_pemesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.update_harga_sewa_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    temp_no_ktp_pemesan VARCHAR(20);
    temp_harga_sewa INTEGER;
    temp_level VARCHAR(20);

  BEGIN
    IF (TG_OP = 'INSERT') THEN
      SELECT p.no_ktp_pemesan INTO temp_no_ktp_pemesan
      FROM pemesanan AS p JOIN barang_pesanan AS bp ON p.id_pemesanan = bp.id_pemesanan
      WHERE p.id_pemesanan = NEW.id_pemesanan;

      SELECT a.level INTO temp_level
      FROM anggota AS a
      WHERE a.no_ktp = temp_no_ktp_pemesan;

      SELECT ibl.harga_sewa INTO temp_harga_sewa
      FROM info_barang_level AS ibl
      WHERE ibl.id_barang = NEW.id_barang AND ibl.nama_level = temp_level;

      UPDATE pemesanan
      SET harga_sewa = harga_sewa + temp_harga_sewa
      WHERE id_pemesanan = NEW.id_pemesanan;
    END IF;
  END;
$$;


ALTER FUNCTION toys_rent.update_harga_sewa_pemesanan() OWNER TO db2018027;

--
-- Name: update_ongkos_pemesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.update_ongkos_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    temp_ongkos INTEGER;

  BEGIN
    IF (TG_OP = 'INSERT') THEN
      SELECT p.ongkos INTO temp_ongkos
      FROM pengiriman AS p
      WHERE p.id_pemesanan = NEW.id_pemesanan;

      UPDATE pemesanan
      SET ongkos = ongkos + temp_ongkos
      WHERE id_pemesanan = NEW.id_pemesanan;
    END IF;
  END;
$$;


ALTER FUNCTION toys_rent.update_ongkos_pemesanan() OWNER TO db2018027;

--
-- Name: update_poin(); Type: FUNCTION; Schema: toys_rent; Owner: db2018027
--

CREATE FUNCTION toys_rent.update_poin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    no_ktp varchar;
  BEGIN
    SELECT A.no_ktp INTO no_ktp
    FROM ANGGOTA AS A
    WHERE A.no_ktp = NEW.no_ktp_pemesan;
    IF (TG_OP = 'INSERT') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.Poin + NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
    IF (TG_OP = 'DELETE') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.poin - NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
    IF (TG_OP = 'UPDATE') THEN
      UPDATE ANGGOTA AS A
        SET A.Poin = A.poin - OLD.Kuantitas_barang * 100 + NEW.Kuantitas_barang * 100
        WHERE A.no_ktp = no_ktp;
      RETURN NEW;
    END IF;
  END
$$;


ALTER FUNCTION toys_rent.update_poin() OWNER TO db2018027;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE toys_rent.admin OWNER TO db2018027;

--
-- Name: alamat; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE toys_rent.alamat OWNER TO db2018027;

--
-- Name: anggota; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE toys_rent.anggota OWNER TO db2018027;

--
-- Name: barang; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);


ALTER TABLE toys_rent.barang OWNER TO db2018027;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10)
);


ALTER TABLE toys_rent.barang_dikembalikan OWNER TO db2018027;

--
-- Name: barang_dikirim; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL
);


ALTER TABLE toys_rent.barang_dikirim OWNER TO db2018027;

--
-- Name: barang_pesanan; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);


ALTER TABLE toys_rent.barang_pesanan OWNER TO db2018027;

--
-- Name: chat; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE toys_rent.chat OWNER TO db2018027;

--
-- Name: info_barang_level; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


ALTER TABLE toys_rent.info_barang_level OWNER TO db2018027;

--
-- Name: item; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


ALTER TABLE toys_rent.item OWNER TO db2018027;

--
-- Name: kategori; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


ALTER TABLE toys_rent.kategori OWNER TO db2018027;

--
-- Name: kategori_item; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE toys_rent.kategori_item OWNER TO db2018027;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.level_keanggotaan OWNER TO db2018027;

--
-- Name: pemesanan; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE toys_rent.pemesanan OWNER TO db2018027;

--
-- Name: pengembalian; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengembalian OWNER TO db2018027;

--
-- Name: pengguna; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE toys_rent.pengguna OWNER TO db2018027;

--
-- Name: pengiriman; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengiriman OWNER TO db2018027;

--
-- Name: status; Type: TABLE; Schema: toys_rent; Owner: db2018027
--

CREATE TABLE toys_rent.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.status OWNER TO db2018027;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.admin (no_ktp) FROM stdin;
240629685
858211789
452592638
992263904
292928141
107067030
996274004
043207256
497396681
586639151
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
939947047	Opaline	Moland	836	Songea	2212
153925799	Prue	Novick	2	Torzhok	8667
647303842	Louella	Cottonwood	3	Hanau	7101
724904049	Ted	Southridge	3	Dongchen	6855
089102197	Davis	Hazelcrest	43	Jianling	1291
134988781	Sheff	Golf	45631	Mahalapye	8834
124477842	Barby	Elka	97	ydzen	9678
577876460	Jules	Sachs	3934	Francisco Sarabia	9423
498726461	Corabelle	Portage	180	Macau	7150
011770897	Carina	Reindahl	275	Rudnik	7547
750472453	Rodolph	Sommers	58	Lanigan	6627
732785464	Alfy	Nova	93	Selebi-Phikwe	8057
619512007	Ulberto	Sundown	7315	Halmstad	9590
788915284	Roarke	Express	219	Dafeng	5627
842160816	Kerwin	Rigney	2	Tarica	7897
114552042	Sayres	Cascade	60127	Wangcheng	9520
985399638	Demetris	Porter	13461	Fos-sur-Mer	9884
707011581	Jae	Algoma	16	Bulungu	8811
865095889	Clayborne	Thackeray	636	Bangelan	3995
216041900	Hyacinth	Monica	7755	Neyagawa	9484
246008789	Bone	Eagan	25432	Savyon	5516
880224783	Katine	Esch	71	Schiedam postbusnummers	7219
678589881	Cher	Tennyson	7	Albuquerque	8446
095340365	Germain	Bartillon	30922	Tando Jm	8353
229259494	Terri-jo	Derek	81	Ariguan	5207
434127112	Leyla	Dryden	800	Uzlovaya	2042
925729193	Beilul	Everett	828	Ngorongoro	4160
293643639	Viva	Dunning	9	Bang Rakam	5673
513486729	Alvera	Ronald Regan	0	Niquero	1848
525906630	Hazlett	Mockingbird	3348	Dbrowa	9336
698030462	Ring	Straubel	24	Partille	8668
472426613	Erek	Ridgeview	5	Ningde	9080
225523370	Andrew	Carioca	503	West Palm Beach	7574
820113345	Brianne	Bobwhite	37	Serra Talhada	3217
410420690	Jeff	Havey	53	Chemiec	9418
281960560	Gal	Di Loreto	34	KaraidelÖ	5534
786824861	Beth	Little Fleur	3	Tajan	1561
953381264	Dolley	Hudson	3956	Sadkowice	3673
922541680	Kerstin	Acker	14840	Aral	1318
675078243	Olivero	Kings	124	Yonggu	5279
144927882	Isis	Derek	60033	Vrzea	9027
185547130	Phaidra	Atwood	3004	Lijiaping	1261
541258873	Bellanca	Burrows	8	Smolenskaya	1358
918957979	Dilly	Harbort	9	Novovorontsovka	6465
728214692	Viviana	Pond	52	Koygorodok	8565
420292387	Aline	Garrison	5477	Banjar Jagasatru	6604
907774323	Roobbie	Center	725	Vinkk	8454
858656795	Catarina	Eastwood	19837	Shar	6604
348485907	Dill	John Wall	3051	Fenglin	2209
024819893	Honor	Troy	8985	Lanot	1099
024819893	Elana	Michigan	4	Ribeira Seca	5373
348485907	Marcelline	Nelson	2	Am	9488
079656409	Lemuel	Farragut	38653	Thß	3470
276939588	Timmie	Dayton	27	Buan	5237
909306853	Barbra	Shelley	744	Baaid	3815
428122162	Carson	Hoffman	518	Sidoger Lebak	4123
635681681	Sandy	Fuller	28	Banyo	7604
215632809	Lindie	La Follette	4	Aginskoye	3170
393546651	Bevvy	Northview	1	Yaviza	1784
865932855	Frederick	Debs	547	Leticia	8837
311001157	Glenn	Sachs	64	Huozhuangzi	1932
290484371	Kleon	Fremont	0	Merapit	6656
058210625	Arman	Blaine	7	Tobat¡	3196
020551601	Pooh	Sycamore	1967	Cacequi	2358
411153041	Dorrie	Dexter	789	Palaichora	2560
176917454	Bay	Del Mar	82821	Agen	3851
527189614	Haywood	Grasskamp	7	Kardamís	4213
064616769	Karissa	La Follette	5424	Leigongjian	1960
982094619	Guido	Fieldstone	193	Tatariv	1958
567080956	Crista	Merrick	5740	Zengji	7933
277465053	Deina	Tennessee	43	Ljungsbro	8647
860746627	Georgena	Blackbird	1	Tambakrejo	1939
815849463	Marlane	Prairie Rose	91987	Sampagar	3138
072378839	Yorke	4th	13	La Unin	4413
042520519	Delphinia	Clyde Gallagher	5	Lanling	9844
362359843	Esteban	Erie	21213	Gadingrejo	5897
594044471	Brynne	Bowman	6494	Cu Git	7215
208035618	Delcina	Talmadge	45422	Fika	4367
348267153	Hedvig	Nevada	268	Manevychi	3761
227991097	Ricky	John Wall	961	Pjani	5988
495124166	Obidiah	Bunting	29	Doln	2893
646745307	Sissie	Doe Crossing	75569	Timon	6781
273118631	Lucia	Cardinal	872	Kademangan	6651
879223800	Trula	Melvin	5	Jianchang	8185
113631010	Ysabel	Union	7759	Yungaisi	7255
199675243	Shana	Del Mar	63	Zhangdiyingzi	9217
708591408	Rafaellle	Village Green	3	East End	5518
626460056	Prissie	Harper	7306	Dipayal	6596
758485034	Terry	Laurel	8913	Cricima	5993
563763171	Junette	Cascade	41	Pingshan	7582
630567791	Bianca	Swallow	594	Rijeka	4307
360665587	Rodrick	Mariners Cove	113	Farn	1170
858656795	Fidelia	Granby	616	Lukou	8064
907774323	Flossie	Carberry	0	Zhongxin	5927
420292387	Rochell	Forest Run	50042	Nice	4864
728214692	Mort	8th	4285	Sintansin	6871
918957979	Marcello	Schiller	289	Dingdian	2863
541258873	Elaina	Mayfield	3	Besko	7610
185547130	Andrew	Main	1	Cuozhou	8275
144927882	Jobey	Farwell	12	Muroran	8274
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.anggota (no_ktp, poin, level) FROM stdin;
276939588	35	Bronze
909306853	36	Bronze
428122162	37	Bronze
635681681	38	Bronze
215632809	39	Bronze
393546651	40	Bronze
865932855	41	Bronze
311001157	42	Bronze
290484371	43	Bronze
058210625	44	Bronze
020551601	45	Bronze
411153041	46	Bronze
176917454	47	Bronze
527189614	48	Bronze
064616769	49	Bronze
982094619	50	Bronze
567080956	51	Bronze
277465053	52	Bronze
860746627	53	Bronze
815849463	54	Bronze
072378839	55	Bronze
042520519	56	Bronze
362359843	57	Bronze
594044471	58	Bronze
208035618	59	Bronze
348267153	60	Bronze
227991097	61	Bronze
495124166	62	Bronze
646745307	63	Bronze
273118631	64	Bronze
879223800	65	Bronze
113631010	66	Bronze
199675243	67	Bronze
708591408	150	Silver
626460056	151	Silver
758485034	152	Silver
563763171	153	Silver
630567791	154	Silver
360665587	155	Silver
939947047	156	Silver
153925799	157	Silver
647303842	158	Silver
724904049	159	Silver
089102197	160	Silver
134988781	161	Silver
434127112	181	Silver
925729193	182	Silver
953381264	270	Gold
922541680	271	Gold
675078243	272	Gold
144927882	273	Gold
185547130	274	Gold
541258873	275	Gold
918957979	276	Gold
728214692	277	Gold
420292387	278	Gold
124477842	162	Silver
577876460	163	Silver
498726461	164	Silver
011770897	165	Silver
750472453	166	Silver
732785464	167	Silver
619512007	168	Silver
788915284	169	Silver
842160816	170	Silver
114552042	171	Silver
985399638	172	Silver
707011581	173	Silver
865095889	174	Silver
216041900	175	Silver
246008789	176	Silver
880224783	177	Silver
678589881	178	Silver
095340365	179	Silver
229259494	180	Silver
079656409	34	Bronze
907774323	279	Gold
293643639	260	Gold
513486729	261	Gold
525906630	262	Gold
698030462	263	Gold
472426613	264	Gold
225523370	265	Gold
820113345	266	Gold
410420690	267	Gold
281960560	268	Gold
786824861	269	Gold
858656795	280	Gold
348485907	281	Gold
024819893	282	Gold
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
1	Boneka Diva	Kuning	http://dummyimage.com/305x448.png/5fa2dd/ffffff	Bekas	23	393546651
2	Boneka Diva	Coklat	http://dummyimage.com/401x410.bmp/cc0000/ffffff	Bekas	48	907774323
3	Boneka Diva	Merah tua	http://dummyimage.com/334x435.png/5fa2dd/ffffff	Bekas	80	229259494
4	Boneka Diva	Biru	http://dummyimage.com/311x256.png/ff4444/ffffff	Bekas	94	199675243
5	Teddy Bear	Coklat	http://dummyimage.com/452x441.jpg/5fa2dd/ffffff	Bekas	91	227991097
6	Teddy Bear	Nila	http://dummyimage.com/335x392.jpg/cc0000/ffffff	Bekas	65	880224783
7	Teddy Bear	Merah muda	http://dummyimage.com/465x272.jpg/dddddd/000000	Bekas	15	750472453
8	Teddy Bear	Merah tua	http://dummyimage.com/370x483.jpg/dddddd/000000	Bekas	24	153925799
9	Hotwheels 95 Mazda RX-7	Coklat muda	http://dummyimage.com/440x256.bmp/cc0000/ffffff	Bekas	89	072378839
10	Hotwheels 95 Mazda RX-7	Nila	http://dummyimage.com/361x466.jpg/5fa2dd/ffffff	Bekas	29	918957979
11	Hotwheels 95 Mazda RX-7	Hijau	http://dummyimage.com/331x323.jpg/ff4444/ffffff	Bekas	13	290484371
12	Hotwheels 95 Mazda RX-7	Coklat muda	http://dummyimage.com/388x402.png/ff4444/ffffff	Bekas	98	724904049
13	Disney Flying Dumbo and Circus	Jingga	http://dummyimage.com/320x382.bmp/cc0000/ffffff	Bekas	100	860746627
14	Disney Flying Dumbo and Circus	Ungu	http://dummyimage.com/360x435.bmp/cc0000/ffffff	Bekas	12	072378839
15	Disney Flying Dumbo and Circus	Emas	http://dummyimage.com/322x270.png/dddddd/000000	Bekas	100	907774323
16	Disney Flying Dumbo and Circus	Merah tua	http://dummyimage.com/338x350.jpg/dddddd/000000	Bekas	81	525906630
17	Yaya Tayo	Merah	http://dummyimage.com/384x322.jpg/dddddd/000000	Bekas	23	024819893
18	Yaya Tayo	Merah tua	http://dummyimage.com/286x341.bmp/ff4444/ffffff	Bekas	37	208035618
19	Yaya Tayo	Hijau	http://dummyimage.com/436x390.png/dddddd/000000	Bekas	94	879223800
20	Yaya Tayo	Ungu	http://dummyimage.com/303x387.jpg/dddddd/000000	Bekas	73	215632809
21	Titanic Rudianto	Jingga	http://dummyimage.com/460x472.png/cc0000/ffffff	Bekas	72	707011581
22	Titanic Rudianto	Coklat muda	http://dummyimage.com/415x368.jpg/ff4444/ffffff	Bekas	48	925729193
23	Titanic Rudianto	Merah tua	http://dummyimage.com/446x330.jpg/5fa2dd/ffffff	Bekas	48	815849463
24	Titanic Rudianto	Biru	http://dummyimage.com/290x267.jpg/5fa2dd/ffffff	Bekas	45	635681681
25	RC Helicopter HX718	Merah tua	http://dummyimage.com/311x481.jpg/ff4444/ffffff	Bekas	35	020551601
26	RC Helicopter HX718	Jingga	http://dummyimage.com/291x356.jpg/ff4444/ffffff	Bekas	35	089102197
27	RC Helicopter HX718	Nila	http://dummyimage.com/500x381.jpg/dddddd/000000	Baru	8	724904049
28	RC Helicopter HX718	Biru	http://dummyimage.com/483x495.jpg/5fa2dd/ffffff	Bekas	56	495124166
29	Pooh Puzzle	Jingga	http://dummyimage.com/438x398.bmp/cc0000/ffffff	Bekas	59	567080956
30	Pooh Puzzle	Ungu	http://dummyimage.com/314x329.jpg/dddddd/000000	Bekas	89	647303842
31	Pooh Puzzle	Hijau	http://dummyimage.com/442x287.png/5fa2dd/ffffff	Bekas	44	880224783
32	Pooh Puzzle	Hijau	http://dummyimage.com/402x326.png/5fa2dd/ffffff	Bekas	30	348267153
33	Talking Hamster Toy	Hijau	http://dummyimage.com/399x483.png/dddddd/000000	Bekas	95	362359843
34	Talking Hamster Toy	Nila	http://dummyimage.com/496x414.png/ff4444/ffffff	Bekas	36	541258873
35	Talking Hamster Toy	Emas	http://dummyimage.com/362x424.png/cc0000/ffffff	Bekas	49	273118631
36	Talking Hamster Toy	Biru tua	http://dummyimage.com/498x492.png/dddddd/000000	Bekas	35	577876460
37	Motoran Mini Harley	Merah muda	http://dummyimage.com/423x374.bmp/5fa2dd/ffffff	Bekas	26	563763171
38	Motoran Mini Harley	Coklat	http://dummyimage.com/257x250.jpg/ff4444/ffffff	Bekas	27	042520519
39	Motoran Mini Harley	Hijau	http://dummyimage.com/436x398.jpg/5fa2dd/ffffff	Baru	5	982094619
40	Motoran Mini Harley	Coklat	http://dummyimage.com/418x257.jpg/cc0000/ffffff	Bekas	37	865095889
41	Thousand Sunny Kapal One Piece	Coklat muda	http://dummyimage.com/361x274.jpg/5fa2dd/ffffff	Bekas	36	842160816
42	Thousand Sunny Kapal One Piece	Biru tua	http://dummyimage.com/436x317.png/cc0000/ffffff	Bekas	52	420292387
43	Thousand Sunny Kapal One Piece	Biru tua	http://dummyimage.com/497x374.jpg/cc0000/ffffff	Bekas	85	842160816
44	Thousand Sunny Kapal One Piece	Merah tua	http://dummyimage.com/362x361.png/5fa2dd/ffffff	Bekas	69	815849463
45	Hotwheels Color Shifter	Merah tua	http://dummyimage.com/384x290.bmp/dddddd/000000	Bekas	97	024819893
46	Hotwheels Color Shifter	Merah tua	http://dummyimage.com/270x425.bmp/dddddd/000000	Bekas	91	281960560
47	Hotwheels Color Shifter	Emas	http://dummyimage.com/251x289.jpg/5fa2dd/ffffff	Bekas	92	918957979
48	Hotwheels Color Shifter	Merah muda	http://dummyimage.com/323x278.jpg/5fa2dd/ffffff	Bekas	58	064616769
49	Tomtomo Land Range Rover Evoque	Merah muda	http://dummyimage.com/453x306.jpg/cc0000/ffffff	Bekas	86	646745307
50	Tomtomo Land Range Rover Evoque	Biru tua	http://dummyimage.com/458x323.jpg/ff4444/ffffff	Baru	8	646745307
51	Tomtomo Land Range Rover Evoque	Emas	http://dummyimage.com/467x255.bmp/cc0000/ffffff	Bekas	63	626460056
52	Tomtomo Land Range Rover Evoque	Biru	http://dummyimage.com/446x475.bmp/5fa2dd/ffffff	Bekas	86	909306853
53	Motor Cross	Biru muda	http://dummyimage.com/474x334.png/ff4444/ffffff	Bekas	47	434127112
54	Motor Cross	Orange	http://dummyimage.com/338x463.jpg/ff4444/ffffff	Bekas	30	619512007
55	Motor Cross	Ungu	http://dummyimage.com/297x406.png/5fa2dd/ffffff	Bekas	88	675078243
56	Motor Cross	Biru muda	http://dummyimage.com/418x462.jpg/dddddd/000000	Baru	5	594044471
57	L.A. Puzzle	Emas	http://dummyimage.com/372x368.bmp/5fa2dd/ffffff	Bekas	43	277465053
58	L.A. Puzzle	Merah	http://dummyimage.com/357x467.png/dddddd/000000	Bekas	47	225523370
59	L.A. Puzzle	Nila	http://dummyimage.com/332x494.bmp/cc0000/ffffff	Bekas	10	011770897
60	L.A. Puzzle	Orange	http://dummyimage.com/378x334.bmp/5fa2dd/ffffff	Bekas	49	982094619
61	Miniatur Stadion Puzzle 3D Juventus Stadium	Nila	http://dummyimage.com/277x386.jpg/5fa2dd/ffffff	Bekas	20	842160816
62	Miniatur Stadion Puzzle 3D Juventus Stadium	Merah tua	http://dummyimage.com/308x477.jpg/5fa2dd/ffffff	Bekas	52	820113345
63	Miniatur Stadion Puzzle 3D Juventus Stadium	Merah muda	http://dummyimage.com/280x316.png/ff4444/ffffff	Bekas	33	909306853
64	Miniatur Stadion Puzzle 3D Juventus Stadium	Merah	http://dummyimage.com/438x361.bmp/5fa2dd/ffffff	Bekas	35	678589881
65	Jigsaw Puzzle Mini Hewan	Hijau	http://dummyimage.com/466x323.jpg/dddddd/000000	Bekas	73	276939588
66	Jigsaw Puzzle Mini Hewan	Jingga	http://dummyimage.com/349x440.png/ff4444/ffffff	Bekas	87	982094619
67	Jigsaw Puzzle Mini Hewan	Biru muda	http://dummyimage.com/455x306.bmp/5fa2dd/ffffff	Bekas	13	293643639
68	Jigsaw Puzzle Mini Hewan	Biru muda	http://dummyimage.com/483x484.png/cc0000/ffffff	Bekas	80	909306853
69	Mobilan Graffiti	Hijau	http://dummyimage.com/258x350.png/dddddd/000000	Bekas	87	124477842
70	Mobilan Graffiti	Emas	http://dummyimage.com/327x326.bmp/5fa2dd/ffffff	Bekas	70	216041900
71	Mobilan Graffiti	Merah muda	http://dummyimage.com/422x253.png/5fa2dd/ffffff	Bekas	86	348485907
72	Mobilan Graffiti	Biru	http://dummyimage.com/315x346.png/cc0000/ffffff	Bekas	74	525906630
73	Hotwheels XB	Merah muda	http://dummyimage.com/381x286.png/dddddd/000000	Bekas	97	199675243
74	Hotwheels XB	Ungu	http://dummyimage.com/308x346.bmp/ff4444/ffffff	Bekas	66	563763171
75	Hotwheels XB	Coklat muda	http://dummyimage.com/400x424.png/dddddd/000000	Bekas	81	246008789
76	Hotwheels XB	Merah	http://dummyimage.com/433x405.png/cc0000/ffffff	Bekas	94	208035618
77	RC Radio Racing Police Jeep Car	Biru	http://dummyimage.com/409x467.png/ff4444/ffffff	Bekas	28	362359843
78	RC Radio Racing Police Jeep Car	Merah muda	http://dummyimage.com/387x443.jpg/5fa2dd/ffffff	Bekas	99	420292387
79	RC Radio Racing Police Jeep Car	Biru	http://dummyimage.com/295x474.bmp/5fa2dd/ffffff	Bekas	70	758485034
80	RC Radio Racing Police Jeep Car	Coklat	http://dummyimage.com/372x399.png/ff4444/ffffff	Bekas	38	865932855
81	Thomas to Robot Transform Manual	Kuning	http://dummyimage.com/432x392.jpg/ff4444/ffffff	Bekas	11	215632809
82	Thomas to Robot Transform Manual	Hijau	http://dummyimage.com/430x459.png/dddddd/000000	Bekas	88	708591408
83	Thomas to Robot Transform Manual	Coklat	http://dummyimage.com/445x396.bmp/dddddd/000000	Bekas	56	865095889
84	Thomas to Robot Transform Manual	Merah tua	http://dummyimage.com/378x342.jpg/5fa2dd/ffffff	Bekas	28	360665587
85	Robocar Poli PIP P3	Ungu	http://dummyimage.com/337x285.bmp/5fa2dd/ffffff	Bekas	93	428122162
86	Robocar Poli PIP P3	Biru tua	http://dummyimage.com/315x453.png/ff4444/ffffff	Bekas	94	925729193
87	Robocar Poli PIP P3	Ungu	http://dummyimage.com/263x312.bmp/ff4444/ffffff	Bekas	49	273118631
88	Robocar Poli PIP P3	Merah	http://dummyimage.com/298x297.bmp/dddddd/000000	Bekas	24	647303842
89	One Piece Jigsaw Puzzle	Merah tua	http://dummyimage.com/346x443.bmp/cc0000/ffffff	Baru	7	072378839
90	One Piece Jigsaw Puzzle	Nila	http://dummyimage.com/405x311.png/cc0000/ffffff	Baru	8	428122162
91	One Piece Jigsaw Puzzle	Coklat muda	http://dummyimage.com/393x418.bmp/ff4444/ffffff	Baru	6	225523370
92	One Piece Jigsaw Puzzle	Emas	http://dummyimage.com/288x379.png/ff4444/ffffff	Bekas	97	434127112
93	124 CHARGER Mobil Remote Contro	Jingga	http://dummyimage.com/257x460.png/ff4444/ffffff	Bekas	90	865932855
94	124 CHARGER Mobil Remote Contro	Merah muda	http://dummyimage.com/256x419.png/cc0000/ffffff	Bekas	72	732785464
95	124 CHARGER Mobil Remote Contro	Hijau	http://dummyimage.com/482x428.bmp/5fa2dd/ffffff	Bekas	77	635681681
96	124 CHARGER Mobil Remote Contro	Merah muda	http://dummyimage.com/316x392.png/dddddd/000000	Bekas	100	820113345
97	Audi R Series	Biru tua	http://dummyimage.com/494x336.bmp/cc0000/ffffff	Bekas	35	208035618
98	Audi R Series	Biru muda	http://dummyimage.com/495x466.jpg/ff4444/ffffff	Bekas	43	563763171
99	Audi R Series	Coklat muda	http://dummyimage.com/348x279.jpg/dddddd/000000	Bekas	69	728214692
100	Audi R Series	Hijau	http://dummyimage.com/250x477.bmp/ff4444/ffffff	Bekas	51	985399638
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
6436011309	1	41
6436011309	2	42
6310971666	1	43
6310971666	2	44
5012525432	1	45
5012525432	2	46
6831075152	1	47
6831075152	2	48
3382903462	1	49
3382903462	2	50
8916887775	1	51
8916887775	2	52
5469480667	1	53
5469480667	2	54
6149629751	1	55
6149629751	2	56
9880654543	1	57
9880654543	2	58
1020250490	1	59
1020250490	2	60
6595966046	1	61
6595966046	2	62
8403126622	1	63
8403126622	2	64
8827401220	1	65
8827401220	2	66
3928582564	1	67
3928582564	2	68
4708636216	1	69
4708636216	2	70
8491406339	1	71
8491406339	2	72
2904061705	1	73
2904061705	2	74
7263872987	1	75
7263872987	2	76
8463127017	1	77
8463127017	2	78
8402740543	1	79
8402740543	2	80
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
9820419838	1	1	2019-04-06	bagus
9820419838	2	21	2019-04-02	ashiyap bgt dah ni barang
8350429893	1	2	2019-03-25	mantap
8350429893	2	22	2019-03-17	untung beli disini, lapak lain mahal
4969006742	1	3	2019-04-11	pengiriman cepet gan
4969006742	2	23	2019-04-10	seller terpercaya
6341863690	1	4	2019-04-12	rekomen bgt nih
6341863690	2	24	2019-03-15	fast respond :D
9271326979	1	5	2019-03-16	barang sesuai gambar
9271326979	2	25	2019-03-20	mantap sellernya
1491417909	1	6	2019-04-07	penjual ramah
1491417909	2	26	2019-03-29	bagus
1719247920	1	7	2019-03-27	jgn ragu lagi beli disini
1719247920	2	27	2019-03-17	mantap
4620436682	1	8	2019-03-29	ashiyap bgt dah ni barang
4620436682	2	28	2019-03-21	pengiriman cepet gan
5715170957	1	9	2019-04-09	untung beli disini, lapak lain mahal
5715170957	2	29	2019-03-30	rekomen bgt nih
8819647529	1	10	2019-03-29	seller terpercaya
8819647529	2	30	2019-03-31	barang sesuai gambar
9262129511	1	11	2019-03-25	fast respond :D
9262129511	2	31	2019-04-07	penjual ramah
1236118554	1	12	2019-04-09	mantap sellernya
1236118554	2	32	2019-03-12	jgn ragu lagi beli disini
1601546268	1	13	2019-03-19	demi basdat, saya ngarang2 :v
1601546268	2	33	2019-03-21	ashiyap bgt dah ni barang
9523056370	1	14	2019-04-12	bagus
9523056370	2	34	2019-04-11	untung beli disini, lapak lain mahal
2490150891	1	15	2019-03-16	mantap
2490150891	2	35	2019-04-06	seller terpercaya
7345200162	1	16	2019-03-18	pengiriman cepet gan
7345200162	2	36	2019-03-12	fast respond :D
3680293827	1	17	2019-04-02	rekomen bgt nih
3680293827	2	37	2019-04-10	mantap sellernya
2409049091	1	18	2019-03-23	barang sesuai gambar
2409049091	2	38	2019-03-29	pengiriman cepet gan
7615550991	1	19	2019-04-10	penjual ramah
7615550991	2	39	2019-03-25	rekomen bgt nih
8156689482	1	20	2019-03-29	jgn ragu lagi beli disini
8156689482	2	40	2019-03-17	barang sesuai gambar
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
id_pesan1	1	1	2019-01-19	30	2019-02-18	Status2
id_pesan1	2	2	2018-11-04	94	2019-02-06	Status4
id_pesan2	1	3	2018-09-27	88	2018-12-24	Status2
id_pesan2	2	4	2018-12-12	25	2019-01-06	Status5
id_pesan3	1	5	2019-02-21	59	2019-04-21	Status5
id_pesan3	2	6	2019-02-09	12	2019-02-21	Status3
id_pesan4	1	7	2018-07-08	100	2018-10-16	Status3
id_pesan4	2	8	2018-09-26	49	2018-11-14	Status2
id_pesan5	1	9	2018-04-26	14	2018-05-10	Status4
id_pesan5	2	10	2019-02-14	40	2019-03-26	Status3
id_pesan6	1	11	2018-06-22	74	2018-09-04	Status5
id_pesan6	2	12	2018-09-05	83	2018-11-27	Status1
id_pesan7	1	13	2018-06-06	89	2018-09-03	Status1
id_pesan7	2	14	2018-06-14	90	2018-09-12	Status2
id_pesan8	1	15	2018-06-21	60	2018-08-20	Status6
id_pesan8	2	16	2019-04-12	97	2019-07-18	Status6
id_pesan9	1	17	2018-09-11	49	2018-10-30	Status3
id_pesan9	2	18	2018-10-23	13	2018-11-05	Status1
id_pesan10	1	19	2018-10-04	32	2018-11-05	Status5
id_pesan10	2	20	2018-11-30	79	2019-02-17	Status6
id_pesan11	1	21	2019-03-25	49	2019-05-13	Status5
id_pesan11	2	22	2018-06-24	24	2018-07-18	Status4
id_pesan12	1	23	2018-12-29	23	2019-01-21	Status3
id_pesan12	2	24	2019-04-05	12	2019-04-17	Status1
id_pesan13	1	25	2018-06-21	20	2018-07-11	Status5
id_pesan13	2	26	2018-11-27	92	2019-02-27	Status4
id_pesan14	1	27	2018-09-09	8	2018-09-17	Status1
id_pesan14	2	28	2018-10-30	76	2019-01-14	Status4
id_pesan15	1	29	2019-03-15	79	2019-06-02	Status4
id_pesan15	2	30	2018-11-22	90	2019-02-20	Status6
id_pesan16	1	31	2018-06-25	43	2018-08-07	Status4
id_pesan16	2	32	2018-12-17	85	2019-03-12	Status5
id_pesan17	1	33	2019-03-18	63	2019-05-20	Status5
id_pesan17	2	34	2018-09-21	8	2018-09-29	Status1
id_pesan18	1	35	2019-04-06	66	2019-06-11	Status4
id_pesan18	2	36	2018-06-16	63	2018-08-18	Status1
id_pesan19	1	37	2018-10-07	20	2018-10-27	Status4
id_pesan19	2	38	2018-08-14	56	2018-10-09	Status4
id_pesan20	1	39	2018-11-07	20	2018-11-27	Status3
id_pesan20	2	40	2018-12-18	19	2019-01-06	Status2
id_pesan21	1	41	2018-09-14	55	2018-11-08	Status1
id_pesan21	2	42	2019-02-24	13	2019-03-09	Status5
id_pesan22	1	43	2018-10-09	73	2018-12-21	Status6
id_pesan22	2	44	2019-01-07	49	2019-02-25	Status6
id_pesan23	1	45	2019-04-06	15	2019-04-21	Status2
id_pesan23	2	46	2018-06-28	12	2018-07-10	Status6
id_pesan24	1	47	2018-09-23	36	2018-10-29	Status4
id_pesan24	2	48	2018-09-06	36	2018-10-12	Status1
id_pesan25	1	49	2019-01-05	19	2019-01-24	Status3
id_pesan25	2	50	2018-10-11	56	2018-12-06	Status6
id_pesan26	1	51	2018-09-05	52	2018-10-27	Status4
id_pesan26	2	52	2018-11-16	100	2019-02-24	Status3
id_pesan27	1	53	2019-02-01	25	2019-02-26	Status5
id_pesan27	2	54	2019-01-08	30	2019-02-07	Status6
id_pesan28	1	55	2018-09-25	95	2018-12-29	Status5
id_pesan28	2	56	2019-01-18	85	2019-04-13	Status4
id_pesan29	1	57	2019-01-21	27	2019-02-17	Status4
id_pesan29	2	58	2018-07-14	37	2018-08-20	Status5
id_pesan30	1	59	2018-04-19	74	2018-07-02	Status4
id_pesan30	2	60	2018-10-27	46	2018-12-12	Status1
id_pesan31	1	61	2019-04-11	32	2019-05-13	Status2
id_pesan31	2	62	2018-05-13	49	2018-07-01	Status5
id_pesan32	1	63	2018-11-30	73	2019-02-11	Status1
id_pesan32	2	64	2018-05-19	99	2018-08-26	Status4
id_pesan33	1	65	2018-08-19	10	2018-08-29	Status4
id_pesan33	2	66	2018-10-26	33	2018-11-28	Status4
id_pesan34	1	67	2018-04-20	72	2018-07-01	Status2
id_pesan34	2	68	2018-12-14	55	2019-02-07	Status3
id_pesan35	1	69	2018-07-25	93	2018-10-26	Status2
id_pesan35	2	70	2019-02-24	35	2019-03-31	Status5
id_pesan36	1	71	2018-12-13	89	2019-03-12	Status3
id_pesan36	2	72	2018-07-29	52	2018-09-19	Status6
id_pesan37	1	73	2018-05-19	29	2018-06-17	Status5
id_pesan37	2	74	2018-09-30	89	2018-12-28	Status3
id_pesan38	1	75	2018-10-25	26	2018-11-20	Status6
id_pesan38	2	76	2019-02-11	84	2019-05-06	Status2
id_pesan39	1	77	2018-04-13	6	2018-04-19	Status4
id_pesan39	2	78	2018-07-17	55	2018-09-10	Status5
id_pesan40	1	79	2018-09-06	26	2018-10-02	Status3
id_pesan40	2	80	2018-05-24	3	2018-05-27	Status5
id_pesan41	1	81	2018-06-08	22	2018-06-30	Status6
id_pesan41	2	82	2018-04-21	71	2018-07-01	Status4
id_pesan42	1	83	2018-05-25	47	2018-07-11	Status5
id_pesan42	2	84	2018-11-05	75	2019-01-19	Status6
id_pesan43	1	85	2018-09-24	74	2018-12-07	Status5
id_pesan43	2	86	2018-10-11	77	2018-12-27	Status1
id_pesan44	1	87	2018-10-25	81	2019-01-14	Status5
id_pesan44	2	88	2018-08-03	73	2018-10-15	Status6
id_pesan45	1	89	2018-07-21	34	2018-08-24	Status5
id_pesan45	2	90	2018-09-02	7	2018-09-09	Status4
id_pesan46	1	91	2018-04-23	16	2018-05-09	Status5
id_pesan46	2	92	2019-02-11	40	2019-03-23	Status1
id_pesan47	1	93	2018-04-17	61	2018-06-17	Status6
id_pesan47	2	94	2019-02-22	6	2019-02-28	Status4
id_pesan48	1	95	2019-03-31	90	2019-06-29	Status2
id_pesan48	2	96	2018-04-29	59	2018-06-27	Status1
id_pesan49	1	97	2018-05-10	9	2018-05-19	Status3
id_pesan49	2	98	2018-09-17	29	2018-10-16	Status5
id_pesan50	1	99	2018-11-17	31	2018-12-18	Status3
id_pesan50	2	100	2018-05-28	32	2018-06-29	Status6
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
1	haloooo	2018-08-15 00:00:00	058210625	586639151
2	ini kenapa ga bisa ya?	2019-04-03 00:00:00	925729193	452592638
3	eh kalau mau beli barang caranya gimana?	2018-06-04 00:00:00	786824861	043207256
4	KERENNNNNN	2018-07-07 00:00:00	393546651	240629685
5	Ini apaan min?	2018-04-27 00:00:00	858656795	992263904
6	Jangan melanggar aturan ketika bertransaksi ya	2018-08-08 00:00:00	114552042	497396681
7	Min, tugas gw banyak	2018-09-30 00:00:00	411153041	292928141
8	klo mau liat profil gimana ya?	2018-05-26 00:00:00	360665587	240629685
9	bedanya NOMOR RESI sama RESI apa?	2019-03-28 00:00:00	348485907	452592638
10	Gimana caranya agar saya bisa naik jadi Silver?	2018-09-21 00:00:00	348267153	043207256
11	Coba tanyain ke customer supportnya langsung ya	2019-03-31 00:00:00	858656795	858211789
12	Ketipu kamu!	2018-05-02 00:00:00	393546651	292928141
13	Saya memprediksi ini adalah chat dengan ID 13	2019-02-03 00:00:00	410420690	043207256
14	Apakah apps ini akan diupdate secara rutin?	2018-04-17 00:00:00	216041900	996274004
15	Maaf karena saya kurang bisa membantu	2018-08-31 00:00:00	698030462	043207256
16	hehehe	2018-11-15 00:00:00	594044471	996274004
17	Min, sistem apps ini bagus ya, pasti nilai basdatnya tinggi ya	2018-05-02 00:00:00	842160816	452592638
18	Benar juga.	2019-03-31 00:00:00	208035618	240629685
19	Eh tapi gimana kalau...	2018-06-20 00:00:00	277465053	043207256
20	apakah brg yg rusak hrs gw gnti?	2018-12-29 00:00:00	953381264	586639151
21	kenapa ongkos ny mahal semua :(	2019-03-23 00:00:00	495124166	452592638
22	lupa tadi mau blg ap.. tunggu y	2019-03-01 00:00:00	495124166	996274004
23	beda ny produk ini sama itu apa ya?	2018-05-06 00:00:00	918957979	858211789
24	Bicaranya mohon lebih sopan ya.	2018-06-16 00:00:00	594044471	107067030
25	Wah	2018-12-17 00:00:00	728214692	452592638
26	Appsnya error mulu di laptop gw :( gimana ngatasinny y?	2019-04-07 00:00:00	144927882	240629685
27	Gimana caranya agar saya bisa jadi admin juga?	2018-06-13 00:00:00	428122162	240629685
28	Kamu bisa klik tombol yang terletak di pojok kanan atas untuk mengeceknya.	2018-12-15 00:00:00	393546651	586639151
29	tes	2018-07-12 00:00:00	880224783	497396681
30	Kerja sebagai admin gajinya tinggi ga sih?	2019-03-18 00:00:00	393546651	497396681
31	Apa urusan Anda menanyakan itu?	2019-02-16 00:00:00	647303842	992263904
32	Maaf min	2018-11-21 00:00:00	024819893	996274004
33	Lumayan	2018-07-05 00:00:00	758485034	043207256
34	NANIIIII??	2018-09-05 00:00:00	208035618	996274004
35	Oke	2018-11-09 00:00:00	281960560	996274004
36	ini kok kata ny 404 not found ya?	2019-01-30 00:00:00	728214692	452592638
37	Iya gpp kok	2019-01-14 00:00:00	472426613	292928141
38	klo brg gw g smpe tepat wktu gmna y?	2018-04-26 00:00:00	095340365	858211789
39	Kamu bisa melaporkannya ke salah satu admin jika itu terjadi.	2018-04-27 00:00:00	728214692	292928141
40	penjual ny g resp nih :( gmna y?	2019-03-06 00:00:00	675078243	996274004
41	Apakah barang yang rusak bisa saya kembalikan?	2019-01-03 00:00:00	732785464	240629685
42	ini pesan ke 42, benar tidak?	2018-10-15 00:00:00	728214692	586639151
43	gw menebak nomor ktp lu 586639151	2018-10-06 00:00:00	072378839	586639151
44	Iya benar.	2019-01-05 00:00:00	525906630	858211789
45	Apakah aplikasi ini bekerja dengan baik?	2018-06-21 00:00:00	953381264	452592638
46	print(random.choice[0, 1])	2018-12-15 00:00:00	208035618	586639151
47	Apakah saya bisa meminjam barang lebih dari 5 tahun?	2018-09-03 00:00:00	842160816	043207256
48	hmm... ini fungsinya apa ya?	2018-04-14 00:00:00	708591408	497396681
49	Bisa.	2018-12-11 00:00:00	630567791	240629685
50	capek banget ketik ini weh	2018-07-23 00:00:00	276939588	858211789
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
1	Bronze	20000	0.0500000007
2	Bronze	20000	0.0500000007
3	Bronze	20000	0.0500000007
4	Bronze	20000	0.0500000007
5	Bronze	20000	0.0500000007
6	Bronze	20000	0.0500000007
7	Bronze	20000	0.0500000007
8	Bronze	20000	0.0500000007
9	Bronze	20000	0.0500000007
10	Bronze	20000	0.0500000007
11	Bronze	20000	0.0500000007
12	Bronze	20000	0.0500000007
13	Bronze	20000	0.0500000007
14	Bronze	20000	0.0500000007
15	Bronze	20000	0.0500000007
16	Bronze	20000	0.0500000007
17	Bronze	20000	0.0500000007
18	Bronze	20000	0.0500000007
19	Bronze	20000	0.0500000007
20	Bronze	20000	0.0500000007
21	Bronze	20000	0.0500000007
22	Bronze	20000	0.0500000007
23	Bronze	20000	0.0500000007
24	Bronze	20000	0.0500000007
25	Bronze	20000	0.0500000007
26	Bronze	20000	0.0500000007
27	Bronze	20000	0.0500000007
28	Bronze	20000	0.0500000007
29	Bronze	20000	0.0500000007
30	Bronze	20000	0.0500000007
31	Bronze	20000	0.0500000007
32	Bronze	20000	0.0500000007
33	Bronze	20000	0.0500000007
34	Bronze	20000	0.0500000007
35	Bronze	20000	0.0500000007
36	Bronze	20000	0.0500000007
37	Bronze	20000	0.0500000007
38	Bronze	20000	0.0500000007
39	Bronze	20000	0.0500000007
40	Bronze	20000	0.0500000007
41	Bronze	20000	0.0500000007
42	Bronze	20000	0.0500000007
43	Bronze	20000	0.0500000007
44	Bronze	20000	0.0500000007
45	Bronze	20000	0.0500000007
46	Bronze	20000	0.0500000007
47	Bronze	20000	0.0500000007
48	Bronze	20000	0.0500000007
49	Bronze	20000	0.0500000007
50	Bronze	20000	0.0500000007
51	Bronze	20000	0.0500000007
52	Bronze	20000	0.0500000007
53	Bronze	20000	0.0500000007
54	Bronze	20000	0.0500000007
55	Bronze	20000	0.0500000007
56	Bronze	20000	0.0500000007
57	Bronze	20000	0.0500000007
58	Bronze	20000	0.0500000007
59	Bronze	20000	0.0500000007
60	Bronze	20000	0.0500000007
61	Bronze	20000	0.0500000007
62	Bronze	20000	0.0500000007
63	Bronze	20000	0.0500000007
64	Bronze	20000	0.0500000007
65	Bronze	20000	0.0500000007
66	Bronze	20000	0.0500000007
67	Bronze	20000	0.0500000007
68	Bronze	20000	0.0500000007
69	Bronze	20000	0.0500000007
70	Bronze	20000	0.0500000007
71	Bronze	20000	0.0500000007
72	Bronze	20000	0.0500000007
73	Bronze	20000	0.0500000007
74	Bronze	20000	0.0500000007
75	Bronze	20000	0.0500000007
76	Bronze	20000	0.0500000007
77	Bronze	20000	0.0500000007
78	Bronze	20000	0.0500000007
79	Bronze	20000	0.0500000007
80	Bronze	20000	0.0500000007
81	Bronze	20000	0.0500000007
82	Bronze	20000	0.0500000007
83	Bronze	20000	0.0500000007
84	Bronze	20000	0.0500000007
85	Bronze	20000	0.0500000007
86	Bronze	20000	0.0500000007
87	Bronze	20000	0.0500000007
88	Bronze	20000	0.0500000007
89	Bronze	20000	0.0500000007
90	Bronze	20000	0.0500000007
91	Bronze	20000	0.0500000007
92	Bronze	20000	0.0500000007
93	Bronze	20000	0.0500000007
94	Bronze	20000	0.0500000007
95	Bronze	20000	0.0500000007
96	Bronze	20000	0.0500000007
97	Bronze	20000	0.0500000007
98	Bronze	20000	0.0500000007
99	Bronze	20000	0.0500000007
100	Bronze	20000	0.0500000007
1	Silver	20000	0.100000001
2	Silver	20000	0.100000001
3	Silver	20000	0.100000001
4	Silver	20000	0.100000001
5	Silver	20000	0.100000001
6	Silver	20000	0.100000001
7	Silver	20000	0.100000001
8	Silver	20000	0.100000001
9	Silver	20000	0.100000001
10	Silver	20000	0.100000001
11	Silver	20000	0.100000001
12	Silver	20000	0.100000001
13	Silver	20000	0.100000001
14	Silver	20000	0.100000001
15	Silver	20000	0.100000001
16	Silver	20000	0.100000001
17	Silver	20000	0.100000001
18	Silver	20000	0.100000001
19	Silver	20000	0.100000001
20	Silver	20000	0.100000001
21	Silver	20000	0.100000001
22	Silver	20000	0.100000001
23	Silver	20000	0.100000001
24	Silver	20000	0.100000001
25	Silver	20000	0.100000001
26	Silver	20000	0.100000001
27	Silver	20000	0.100000001
28	Silver	20000	0.100000001
29	Silver	20000	0.100000001
30	Silver	20000	0.100000001
31	Silver	20000	0.100000001
32	Silver	20000	0.100000001
33	Silver	20000	0.100000001
34	Silver	20000	0.100000001
35	Silver	20000	0.100000001
36	Silver	20000	0.100000001
37	Silver	20000	0.100000001
38	Silver	20000	0.100000001
39	Silver	20000	0.100000001
40	Silver	20000	0.100000001
41	Silver	20000	0.100000001
42	Silver	20000	0.100000001
43	Silver	20000	0.100000001
44	Silver	20000	0.100000001
45	Silver	20000	0.100000001
46	Silver	20000	0.100000001
47	Silver	20000	0.100000001
48	Silver	20000	0.100000001
49	Silver	20000	0.100000001
50	Silver	20000	0.100000001
51	Silver	20000	0.100000001
52	Silver	20000	0.100000001
53	Silver	20000	0.100000001
54	Silver	20000	0.100000001
55	Silver	20000	0.100000001
56	Silver	20000	0.100000001
57	Silver	20000	0.100000001
58	Silver	20000	0.100000001
59	Silver	20000	0.100000001
60	Silver	20000	0.100000001
61	Silver	20000	0.100000001
62	Silver	20000	0.100000001
63	Silver	20000	0.100000001
64	Silver	20000	0.100000001
65	Silver	20000	0.100000001
66	Silver	20000	0.100000001
67	Silver	20000	0.100000001
68	Silver	20000	0.100000001
69	Silver	20000	0.100000001
70	Silver	20000	0.100000001
71	Silver	20000	0.100000001
72	Silver	20000	0.100000001
73	Silver	20000	0.100000001
74	Silver	20000	0.100000001
75	Silver	20000	0.100000001
76	Silver	20000	0.100000001
77	Silver	20000	0.100000001
78	Silver	20000	0.100000001
79	Silver	20000	0.100000001
80	Silver	20000	0.100000001
81	Silver	20000	0.100000001
82	Silver	20000	0.100000001
83	Silver	20000	0.100000001
84	Silver	20000	0.100000001
85	Silver	20000	0.100000001
86	Silver	20000	0.100000001
87	Silver	20000	0.100000001
88	Silver	20000	0.100000001
89	Silver	20000	0.100000001
90	Silver	20000	0.100000001
91	Silver	20000	0.100000001
92	Silver	20000	0.100000001
93	Silver	20000	0.100000001
94	Silver	20000	0.100000001
95	Silver	20000	0.100000001
96	Silver	20000	0.100000001
97	Silver	20000	0.100000001
98	Silver	20000	0.100000001
99	Silver	20000	0.100000001
100	Silver	20000	0.100000001
1	Gold	20000	0.150000006
2	Gold	20000	0.150000006
3	Gold	20000	0.150000006
4	Gold	20000	0.150000006
5	Gold	20000	0.150000006
6	Gold	20000	0.150000006
7	Gold	20000	0.150000006
8	Gold	20000	0.150000006
9	Gold	20000	0.150000006
10	Gold	20000	0.150000006
11	Gold	20000	0.150000006
12	Gold	20000	0.150000006
13	Gold	20000	0.150000006
14	Gold	20000	0.150000006
15	Gold	20000	0.150000006
16	Gold	20000	0.150000006
17	Gold	20000	0.150000006
18	Gold	20000	0.150000006
19	Gold	20000	0.150000006
20	Gold	20000	0.150000006
21	Gold	20000	0.150000006
22	Gold	20000	0.150000006
23	Gold	20000	0.150000006
24	Gold	20000	0.150000006
25	Gold	20000	0.150000006
26	Gold	20000	0.150000006
27	Gold	20000	0.150000006
28	Gold	20000	0.150000006
29	Gold	20000	0.150000006
30	Gold	20000	0.150000006
31	Gold	20000	0.150000006
32	Gold	20000	0.150000006
33	Gold	20000	0.150000006
34	Gold	20000	0.150000006
35	Gold	20000	0.150000006
36	Gold	20000	0.150000006
37	Gold	20000	0.150000006
38	Gold	20000	0.150000006
39	Gold	20000	0.150000006
40	Gold	20000	0.150000006
41	Gold	20000	0.150000006
42	Gold	20000	0.150000006
43	Gold	20000	0.150000006
44	Gold	20000	0.150000006
45	Gold	20000	0.150000006
46	Gold	20000	0.150000006
47	Gold	20000	0.150000006
48	Gold	20000	0.150000006
49	Gold	20000	0.150000006
50	Gold	20000	0.150000006
51	Gold	20000	0.150000006
52	Gold	20000	0.150000006
53	Gold	20000	0.150000006
54	Gold	20000	0.150000006
55	Gold	20000	0.150000006
56	Gold	20000	0.150000006
57	Gold	20000	0.150000006
58	Gold	20000	0.150000006
59	Gold	20000	0.150000006
60	Gold	20000	0.150000006
61	Gold	20000	0.150000006
62	Gold	20000	0.150000006
63	Gold	20000	0.150000006
64	Gold	20000	0.150000006
65	Gold	20000	0.150000006
66	Gold	20000	0.150000006
67	Gold	20000	0.150000006
68	Gold	20000	0.150000006
69	Gold	20000	0.150000006
70	Gold	20000	0.150000006
71	Gold	20000	0.150000006
72	Gold	20000	0.150000006
73	Gold	20000	0.150000006
74	Gold	20000	0.150000006
75	Gold	20000	0.150000006
76	Gold	20000	0.150000006
77	Gold	20000	0.150000006
78	Gold	20000	0.150000006
79	Gold	20000	0.150000006
80	Gold	20000	0.150000006
81	Gold	20000	0.150000006
82	Gold	20000	0.150000006
83	Gold	20000	0.150000006
84	Gold	20000	0.150000006
85	Gold	20000	0.150000006
86	Gold	20000	0.150000006
87	Gold	20000	0.150000006
88	Gold	20000	0.150000006
89	Gold	20000	0.150000006
90	Gold	20000	0.150000006
91	Gold	20000	0.150000006
92	Gold	20000	0.150000006
93	Gold	20000	0.150000006
94	Gold	20000	0.150000006
95	Gold	20000	0.150000006
96	Gold	20000	0.150000006
97	Gold	20000	0.150000006
98	Gold	20000	0.150000006
99	Gold	20000	0.150000006
100	Gold	20000	0.150000006
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
Boneka Diva	Boneka Diva The Series dari Kastari Animation Studio.	1	5	Kain
Teddy Bear	Teddy Bear jumbo	3	19	Bulu rusfur
Hotwheels 95 Mazda RX-7	Hotwheels yang diproduksi tahun 2017	2	59	Logam
Disney Flying Dumbo and Circus	1000pcs Jigsaw Puzzle.	6	32	Karton
Yaya Tayo	Mobil-mobilan buatan Korea.	2	11	Plastik
Titanic Rudianto	Kapal miniatur.	5	17	Plastik
RC Helicopter HX718	Remote Control Helikopter 3.5 Channel.	15	29	Besi halus
Pooh Puzzle	Puzzle dari kartun Winnie the Pooh.	7	49	Karton
Talking Hamster Toy	Talking Hamster Toy dapat meniru suara Anda menjadi suara yang imut.	6	14	Kain
Motoran Mini Harley	Letakan di lantai, tarik kebelakang, dan lepaskan maka mainan ini akan berjalan nantinya.	17	98	Plastik
Thousand Sunny Kapal One Piece	Action Figure Mega WCF.	17	53	Polivinil klorida
Hotwheels Color Shifter	Hotwheels ini dapat berubah warna.	14	51	Logam
Tomtomo Land Range Rover Evoque	Mobil ini bermaterial besi halus dengan kualitas yang cukup baik.	5	38	Besi halus
Motor Cross	Mainan motor polisi. Semua berbahan plastik dan bisa dibuat sebagai pajangan.	4	19	Plastik
L.A. Puzzle	Puzzle kota Los Angeles.	17	34	Triplek
Miniatur Stadion Puzzle 3D Juventus Stadium	Ukuran 345mm x 245mm x 95mm.	6	21	Spons keras
Jigsaw Puzzle Mini Hewan	Ukuran 25cm x 19cm.	2	16	Karton
Mobilan Graffiti	Mainan mobil-mobilan yang gagah dengan model big foot ini dapat dijalankan dengan cara pull back.	7	36	Logam, plastik
Hotwheels XB	Hotwheels impor produksi tahun 2018.	4	54	Besi halus
RC Radio Racing Police Jeep Car	Mobil-mobilan ini dapat digerakkan menggunakan remote control.	3	19	Plastik
Thomas to Robot Transform Manual	Kereta Thomas yang dapat berubah menjadi robot.	3	19	Cheese - Ermite Bleu
Robocar Poli PIP P3	Set 4 mobil-mobilan Robocar Poli PIP P3.	5	14	Plastik
One Piece Jigsaw Puzzle	Puzzle yang cocok untuk penggemar One Piece.	7	31	Karton
124 CHARGER Mobil Remote Contro	Mobil-mobilan ini dapat digerakkan menggunakan remote control dan tahan banting.	19	54	Besi
Audi R Series	Mobil-mobilan Sport Diecast.	8	23	Besi halus
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.kategori (nama, level, sub_dari) FROM stdin;
Kendaraan	1	\N
Boneka	1	\N
Puzzle	1	\N
Mobil-mobilan	2	Kendaraan
Motor-motoran	2	Kendaraan
Mobil radio kontrol	3	Mobil-mobilan
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.kategori_item (nama_item, nama_kategori) FROM stdin;
Boneka Diva	Boneka
Boneka Diva	Puzzle
Teddy Bear	Boneka
Teddy Bear	Puzzle
Hotwheels 95 Mazda RX-7	Kendaraan
Hotwheels 95 Mazda RX-7	Boneka
Hotwheels 95 Mazda RX-7	Mobil-mobilan
Hotwheels 95 Mazda RX-7	Motor-motoran
Hotwheels 95 Mazda RX-7	Mobil radio kontrol
Disney Flying Dumbo and Circus	Boneka
Disney Flying Dumbo and Circus	Puzzle
Disney Flying Dumbo and Circus	Mobil-mobilan
Disney Flying Dumbo and Circus	Motor-motoran
Yaya Tayo	Kendaraan
Yaya Tayo	Mobil-mobilan
Yaya Tayo	Motor-motoran
Yaya Tayo	Mobil radio kontrol
Titanic Rudianto	Kendaraan
Titanic Rudianto	Mobil-mobilan
Titanic Rudianto	Motor-motoran
Titanic Rudianto	Mobil radio kontrol
RC Helicopter HX718	Kendaraan
RC Helicopter HX718	Mobil-mobilan
RC Helicopter HX718	Motor-motoran
RC Helicopter HX718	Mobil radio kontrol
Pooh Puzzle	Boneka
Pooh Puzzle	Puzzle
Talking Hamster Toy	Boneka
Talking Hamster Toy	Puzzle
Talking Hamster Toy	Mobil radio kontrol
Motoran Mini Harley	Kendaraan
Motoran Mini Harley	Boneka
Motoran Mini Harley	Puzzle
Motoran Mini Harley	Mobil-mobilan
Motoran Mini Harley	Motor-motoran
Motoran Mini Harley	Mobil radio kontrol
Thousand Sunny Kapal One Piece	Kendaraan
Thousand Sunny Kapal One Piece	Boneka
Thousand Sunny Kapal One Piece	Puzzle
Thousand Sunny Kapal One Piece	Mobil-mobilan
Thousand Sunny Kapal One Piece	Motor-motoran
Thousand Sunny Kapal One Piece	Mobil radio kontrol
Hotwheels Color Shifter	Kendaraan
Hotwheels Color Shifter	Boneka
Hotwheels Color Shifter	Mobil-mobilan
Hotwheels Color Shifter	Motor-motoran
Hotwheels Color Shifter	Mobil radio kontrol
Tomtomo Land Range Rover Evoque	Kendaraan
Tomtomo Land Range Rover Evoque	Puzzle
Tomtomo Land Range Rover Evoque	Mobil-mobilan
Tomtomo Land Range Rover Evoque	Mobil radio kontrol
Motor Cross	Kendaraan
Motor Cross	Boneka
Motor Cross	Motor-motoran
Motor Cross	Mobil radio kontrol
L.A. Puzzle	Boneka
L.A. Puzzle	Puzzle
Miniatur Stadion Puzzle 3D Juventus Stadium	Boneka
Miniatur Stadion Puzzle 3D Juventus Stadium	Puzzle
Jigsaw Puzzle Mini Hewan	Puzzle
Mobilan Graffiti	Kendaraan
Mobilan Graffiti	Mobil-mobilan
Mobilan Graffiti	Motor-motoran
Mobilan Graffiti	Mobil radio kontrol
Hotwheels XB	Kendaraan
Hotwheels XB	Mobil-mobilan
Hotwheels XB	Motor-motoran
Hotwheels XB	Mobil radio kontrol
RC Radio Racing Police Jeep Car	Kendaraan
RC Radio Racing Police Jeep Car	Mobil-mobilan
RC Radio Racing Police Jeep Car	Motor-motoran
RC Radio Racing Police Jeep Car	Mobil radio kontrol
Thomas to Robot Transform Manual	Kendaraan
Thomas to Robot Transform Manual	Mobil-mobilan
Thomas to Robot Transform Manual	Motor-motoran
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
Gold	200	level tertinggi
Silver	100	level tengah-tengah
Bronze	3	level terendah
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
id_pesan1	2019-02-24 00:00:00	49	58740.1289	10299.3799	079656409	Status1
id_pesan2	2019-02-12 00:00:00	56	39506.8906	85521.5234	276939588	Status2
id_pesan3	2019-01-12 00:00:00	87	40193.4297	92917.1562	909306853	Status3
id_pesan4	2019-02-02 00:00:00	22	54982.4883	77642.0312	428122162	Status4
id_pesan5	2019-01-20 00:00:00	24	15103.1504	85433.8516	635681681	Status5
id_pesan6	2019-01-27 00:00:00	7	55092.3789	86397.4766	215632809	Status6
id_pesan7	2019-02-14 00:00:00	29	75793.2578	14612.6504	393546651	Status1
id_pesan8	2019-03-07 00:00:00	49	47653.0195	70047.2969	865932855	Status2
id_pesan9	2019-04-07 00:00:00	59	42224.4609	17628.2402	311001157	Status3
id_pesan10	2019-02-25 00:00:00	40	24759.1602	79772.5625	290484371	Status4
id_pesan11	2019-03-26 00:00:00	67	74866.6719	56172.6797	058210625	Status5
id_pesan12	2019-03-23 00:00:00	97	84984.8203	63263.9219	020551601	Status6
id_pesan13	2019-01-31 00:00:00	38	25870.1309	42048.3594	411153041	Status1
id_pesan14	2019-02-26 00:00:00	29	74438.7422	58663.8203	176917454	Status2
id_pesan15	2019-03-10 00:00:00	48	94843.4297	33173.3594	527189614	Status3
id_pesan16	2019-02-03 00:00:00	67	60529.7188	84113.1094	064616769	Status4
id_pesan17	2019-02-13 00:00:00	91	44931.9102	24460.0898	982094619	Status5
id_pesan18	2019-03-26 00:00:00	77	51449.1289	91151.0469	567080956	Status6
id_pesan19	2019-02-20 00:00:00	4	83367.4609	77595.0078	277465053	Status1
id_pesan20	2019-02-27 00:00:00	54	88014.6328	31588.5605	860746627	Status2
id_pesan21	2019-01-24 00:00:00	80	75204.6094	93553.3438	815849463	Status3
id_pesan22	2019-03-07 00:00:00	30	37482.1797	25666.4492	072378839	Status4
id_pesan23	2019-03-06 00:00:00	50	11476.6797	47787.6094	042520519	Status5
id_pesan24	2019-03-01 00:00:00	81	22599.7695	44199.8516	362359843	Status6
id_pesan25	2019-03-06 00:00:00	44	43362.1211	98833.8281	594044471	Status1
id_pesan26	2019-02-12 00:00:00	20	89488.0625	46547.9492	208035618	Status2
id_pesan27	2019-02-03 00:00:00	79	63557.6094	60398.8594	348267153	Status3
id_pesan28	2019-03-20 00:00:00	83	15638.3701	12019.96	227991097	Status4
id_pesan29	2019-01-12 00:00:00	43	42582.2109	95802.3203	495124166	Status5
id_pesan30	2019-01-17 00:00:00	51	63090.0312	32746.6191	646745307	Status6
id_pesan31	2019-02-09 00:00:00	70	77636.3984	47285.2891	273118631	Status1
id_pesan32	2019-03-17 00:00:00	15	77731.3594	26432.1406	879223800	Status2
id_pesan33	2019-01-22 00:00:00	23	44109.8398	77073.6719	113631010	Status3
id_pesan34	2019-03-09 00:00:00	78	26361.7891	54317.2695	199675243	Status4
id_pesan35	2019-01-31 00:00:00	7	93998.5391	70642	708591408	Status5
id_pesan36	2019-03-23 00:00:00	27	23395.9199	84933.9062	626460056	Status6
id_pesan37	2019-01-23 00:00:00	45	75406.8125	22374.9902	758485034	Status1
id_pesan38	2019-01-17 00:00:00	19	77595.2969	51683.3086	563763171	Status2
id_pesan39	2019-03-04 00:00:00	92	30151.1406	31528.8008	630567791	Status3
id_pesan40	2019-02-27 00:00:00	43	31403.5293	98034.0625	360665587	Status4
id_pesan41	2019-02-21 00:00:00	9	83532.0078	67843.0078	939947047	Status5
id_pesan42	2019-03-15 00:00:00	80	59451.9102	88254	153925799	Status6
id_pesan43	2019-01-28 00:00:00	74	66888.6484	78191.8438	647303842	Status1
id_pesan44	2019-03-26 00:00:00	36	25949.6504	21295.0605	724904049	Status2
id_pesan45	2019-03-07 00:00:00	87	79289.2812	94463.9297	089102197	Status3
id_pesan46	2019-01-13 00:00:00	62	38794.4609	82295.7422	134988781	Status4
id_pesan47	2019-03-16 00:00:00	93	52708.4688	18956.5605	124477842	Status5
id_pesan48	2019-03-27 00:00:00	49	65853.75	21607.4102	577876460	Status6
id_pesan49	2019-02-04 00:00:00	51	71421.0625	98605.3281	498726461	Status1
id_pesan50	2019-02-24 00:00:00	26	93884.25	89198.8438	011770897	Status2
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
6436011309	id_pesan21	UBER	17088.8906	2019-04-14	815849463	Marlane
6310971666	id_pesan22	JNE	17756.1895	2019-03-15	072378839	Yorke
5012525432	id_pesan23	GRAB	18354.5508	2019-04-04	042520519	Delphinia
6831075152	id_pesan24	UBER	18793.7695	2019-04-17	362359843	Esteban
3382903462	id_pesan25	OJEK BIASA	17184.2793	2019-04-09	594044471	Brynne
8916887775	id_pesan26	OJEK BIASA	17170.3594	2019-04-16	208035618	Delcina
5469480667	id_pesan27	UBER	17016.5996	2019-03-23	348267153	Hedvig
6149629751	id_pesan28	GRAB	18591.8691	2019-04-15	227991097	Ricky
9880654543	id_pesan29	GRAB	15099.4805	2019-03-18	495124166	Obidiah
1020250490	id_pesan30	DHL	17130.1309	2019-04-16	646745307	Sissie
6595966046	id_pesan31	DHL	18946.9492	2019-03-31	273118631	Lucia
8403126622	id_pesan32	JNE	18596.8809	2019-04-03	879223800	Trula
8827401220	id_pesan33	DHL	18622.4199	2019-03-16	113631010	Ysabel
3928582564	id_pesan34	JNE	17869.9297	2019-03-27	199675243	Shana
4708636216	id_pesan35	UBER	16864.3906	2019-04-02	708591408	Rafaellle
8491406339	id_pesan36	OJEK BIASA	16335.4297	2019-04-14	626460056	Prissie
2904061705	id_pesan37	DHL	16958.2793	2019-04-13	758485034	Terry
7263872987	id_pesan38	GO-JEK	17880.0801	2019-04-02	563763171	Junette
8463127017	id_pesan39	GO-JEK	17442.8398	2019-04-06	630567791	Bianca
8402740543	id_pesan40	JNE	15451.1201	2019-04-19	360665587	Rodrick
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
079656409	Katuscha Pickance	kpickance0@histats.com	1999-03-13	021874839594
276939588	Maren Tibb	mtibb1@kickstarter.com	2001-07-11	021845140686
909306853	Bond Mounfield	bmounfield2@adobe.com	2005-01-27	021486437884
428122162	Myrna Marskell	mmarskell3@symantec.com	2002-12-11	021769501245
635681681	Garwin Buffy	gbuffy4@erecht24.de	2005-02-02	021155208849
215632809	Karalee Rhymes	krhymes5@canalblog.com	2003-01-31	021843342748
393546651	Abbey Cubbini	acubbini6@boston.com	2002-08-01	021295111145
865932855	Cathleen Feveryear	cfeveryear7@washingtonpost.com	2004-09-13	021793402921
311001157	Maridel Cahillane	mcahillane8@hhs.gov	2003-07-28	021640990007
290484371	Carlynne Lambeth	clambeth9@blogs.com	2001-02-17	021539484881
058210625	Angelita Zellner	azellnera@shutterfly.com	1999-03-02	021108670783
020551601	Ernesto Blas	eblasb@icio.us	2003-07-02	021440130712
411153041	Heywood Paramor	hparamorc@wp.com	2002-04-25	021171294933
176917454	Alla McLernon	amclernond@listmanage.com	2003-09-27	021593566391
527189614	Kordula Fairfull	kfairfulle@weather.com	2004-11-19	021491698204
064616769	Thomasina Fyall	tfyallf@lulu.com	2001-08-10	021631473976
982094619	Cornie Farnaby	cfarnabyg@netscape.com	2002-07-29	021542519606
567080956	Randa Gulc	rgulch@bbb.org	2003-08-01	021172485854
277465053	Nelle Ralls	nrallsi@friendfeed.com	2003-11-27	021880251875
860746627	Kearney Enochsson	kenochssonj@google.co.jp	2003-11-01	021764010561
815849463	Geoffrey Bielby	gbielbyk@sphinn.com	1999-02-07	021624614496
072378839	Jessika Deluce	jdelucel@google.fr	2000-03-25	021540982797
042520519	Lek Friedank	lfriedankm@va.gov	2004-12-31	021688165871
362359843	Peterus Drews	pdrewsn@google.ca	2002-10-05	021189351321
594044471	Urbano Denley	udenleyo@csmonitor.com	2004-10-06	021579700956
208035618	Collete Kirman	ckirmanp@wiley.com	2001-03-17	021222754892
348267153	Elia Tackes	etackesq@a8.net	2003-07-09	021291419633
227991097	Kaitlyn Aliman	kalimanr@domainmarket.com	1999-04-19	021508480928
495124166	Norman Blackeby	nblackebys@oakley.com	2004-06-01	021776290090
646745307	Kristopher Formby	kformbyt@vinaora.com	2001-10-17	021745609553
273118631	Filia McLay	fmclayu@is.gd	2000-05-19	021386332284
879223800	Frankie Dovydenas	fdovydenasv@desdev.cn	2000-11-25	021781532044
113631010	Jillene Wride	jwridew@cisco.com	2000-07-01	021671052064
199675243	Barris Knowles	bknowlesx@cam.ac.uk	2004-02-08	021302138392
708591408	Elston Cuphus	ecuphusy@google.ca	2002-04-27	021728207608
626460056	Prudence Coombs	pcoombsz@linkedin.com	2004-06-12	021258310870
758485034	George Booeln	gbooeln10@hc360.com	1999-10-06	021330868826
563763171	Danella Knapper	dknapper11@altervista.org	2001-07-12	021270898488
630567791	Dedra Lampl	dlampl12@acquirethisname.com	2003-06-23	021778819450
360665587	Sher Fallon	sfallon13@upenn.edu	2002-09-28	021470177365
939947047	Desi Lakey	dlakey14@mayoclinic.com	2004-04-17	021855498017
153925799	Monty Thomson	mthomson15@usda.gov	2002-12-20	021209259313
647303842	Felicle Van De Cappelle	fvan16@upenn.edu	2002-06-29	021557908463
724904049	Jolyn Pidcock	jpidcock17@mozilla.org	2001-09-06	021759012139
089102197	Gun Levy	glevy18@sogou.com	1999-02-23	021750348332
134988781	Norma Fetherston	nfetherston19@tamu.edu	2003-10-30	021632669324
124477842	Florinda Raise	fraise1a@dagondesign.com	1999-09-26	021628126421
577876460	Christy Moody	cmoody1b@usda.gov	2000-12-29	021510895442
498726461	Fay Alejo	falejo1c@army.mil	2001-04-29	021813665951
011770897	Loren Hicklingbottom	lhicklingbottom1d@yellowbook.com	1999-03-05	021660142142
750472453	Berkley Kitlee	bkitlee1e@ihg.com	2001-07-22	021450120429
732785464	Darby Sercombe	dsercombe1f@blog.com	2001-07-22	021858968062
619512007	Loy McKew	lmckew1g@live.com	2002-04-01	021347703078
788915284	Erwin Gunnell	egunnell1h@epa.gov	2001-03-30	021243928959
842160816	Ashli Croan	acroan1i@exblog.jp	2001-07-30	021326149259
114552042	Cortie Greber	cgreber1j@craigslist.org	1999-09-08	021796758751
985399638	Meridith Castelain	mcastelain1k@dion.ne.jp	2004-12-17	021348101984
707011581	Thornton Dyott	tdyott1l@fc2.com	1999-07-21	021718747381
865095889	Myrta Lankham	mlankham1m@earthlink.net	2001-03-15	021416109992
216041900	Filmore Lumsdon	flumsdon1n@samsung.com	2004-10-09	021882954509
246008789	Jake Garn	jgarn1o@a8.net	2002-07-04	021321880817
880224783	Vallie Bondy	vbondy1p@oaic.gov.au	2004-03-15	021252162517
678589881	Zia De Blasi	zde1q@utexas.edu	2000-03-12	021502261350
095340365	Laurent Virgin	lvirgin1r@time.com	2003-06-29	021793220987
229259494	Charlot Thiese	cthiese1s@imageshack.us	2002-01-08	021451425272
434127112	Vinny Arundell	varundell1t@bing.com	2003-05-14	021873479834
925729193	Bayard Keasy	bkeasy1u@prweb.com	2003-11-02	021310829082
293643639	Penny Danher	pdanher1v@marriott.com	2003-03-28	021645652533
513486729	Blanca Geal	bgeal1w@reddit.com	2004-12-05	021667809754
525906630	Cathie Cosgriff	ccosgriff1x@listmanage.com	2001-07-05	021826639638
698030462	Kyle McGahy	kmcgahy1y@moonfruit.com	2003-02-18	021264205748
472426613	August Loins	aloins1z@facebook.com	2002-11-08	021204889883
225523370	Shelba Allom	sallom20@51.la	2000-11-03	021310082346
820113345	Harlin Le Batteur	hle21@paypal.com	2003-01-16	021308873333
410420690	Teodoro MacGruer	tmacgruer22@constantcontact.com	2000-07-24	021489156776
281960560	Perry Amy	pamy23@amazon.com	2000-03-15	021489325089
786824861	Pattie Roust	proust24@phoca.cz	2002-02-16	021339036489
953381264	Brandyn Carlan	bcarlan25@eventbrite.com	2002-02-25	021812969838
922541680	Karna Toombs	ktoombs26@census.gov	2000-01-01	021705828190
675078243	Guillemette Nazair	gnazair27@ftc.gov	2002-05-01	021877657188
144927882	Tina Meadowcraft	tmeadowcraft28@mit.edu	2002-01-31	021224548030
185547130	Ashli Stealey	astealey29@bbb.org	2004-08-17	021266679190
541258873	Helge Plumstead	hplumstead2a@sitemeter.com	2001-05-10	021258638476
918957979	Portie Mixhel	pmixhel2b@yahoo.com	2004-06-05	021597754308
728214692	Adiana MacKnocker	amacknocker2c@vkontakte.ru	2004-04-16	021203569739
420292387	Dodi Schmuhl	dschmuhl2d@simplemachines.org	2003-04-07	021340281890
907774323	Miof mela McTrustram	mmela2e@goodreads.com	2000-10-09	021231546356
858656795	Roderich Franckton	rfranckton2f@diigo.com	2001-07-08	021250018338
348485907	Hanan Thomas	hthomas2g@berkeley.edu	2000-04-09	021843335855
024819893	Saunder Boissier	sboissier2h@typepad.com	2003-08-20	021390301320
240629685	Giacopo Naptine	gnaptine2i@qq.com	1999-09-23	021864365338
858211789	Sarge Crasswell	scrasswell2j@yahoo.com	2002-03-29	021163822613
452592638	Hagan Blaksland	hblaksland2k@usatoday.com	2000-03-09	021551512427
992263904	Rebe Kennerley	rkennerley2l@cyberchimps.com	1999-12-18	021780194736
292928141	Chuck Lynnett	clynnett2m@elegantthemes.com	2003-12-31	021386608697
107067030	Dwain Lente	dlente2n@themeforest.net	2003-10-21	021562927156
996274004	Ailey Gounin	agounin2o@usgs.gov	1999-02-10	021746696815
043207256	Petr Mathys	pmathys2p@microsoft.com	2005-01-15	021400104922
497396681	Jaclyn Niblock	jniblock2q@sonet.ne.jp	1999-02-24	021437398436
586639151	Hewett McKeever	hmckeever2r@smugmug.com	2004-11-20	021598846163
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
9820419838	id_pesan1	OJEK	2290.95898	2019-04-04	079656409	Lemuel
8350429893	id_pesan2	JNE	25754.5469	2018-04-27	276939588	Timmie
4969006742	id_pesan3	JNE	29858.0449	2019-01-18	909306853	Barbra
6341863690	id_pesan4	GRAB	45364.8164	2019-01-06	428122162	Carson
9271326979	id_pesan5	GOJEK	41164.2617	2018-01-15	635681681	Sandy
1491417909	id_pesan6	JNE	2651.00195	2018-06-21	215632809	Lindie
1719247920	id_pesan7	JNE	35465.8672	2019-01-14	393546651	Bevvy
4620436682	id_pesan8	GRAB	12993.6465	2018-12-25	865932855	Frederick
5715170957	id_pesan9	GOJEK	11774.9111	2018-12-30	311001157	Glenn
8819647529	id_pesan10	GOJEK	19260.3418	2018-05-29	290484371	Kleon
9262129511	id_pesan11	GOJEK	17002.6426	2018-11-25	058210625	Arman
1236118554	id_pesan12	GRAB	31653.1152	2018-01-23	020551601	Pooh
1601546268	id_pesan13	OJEK	48571.6641	2018-06-11	411153041	Dorrie
9523056370	id_pesan14	JNE	47637.5352	2018-04-11	176917454	Bay
2490150891	id_pesan15	GRAB	21150.3301	2019-04-12	527189614	Haywood
7345200162	id_pesan16	GRAB	5590.66992	2018-09-29	064616769	Karissa
3680293827	id_pesan17	JNE	498.434998	2018-02-04	982094619	Guido
2409049091	id_pesan18	GOJEK	23505.2188	2018-07-20	567080956	Crista
7615550991	id_pesan19	OJEK	24362.2559	2018-08-19	277465053	Deina
8156689482	id_pesan20	OJEK	23746.2285	2018-04-07	860746627	Georgena
6436011309	id_pesan21	OJEK	31397.7012	2019-02-20	815849463	Marlane
6310971666	id_pesan22	GOJEK	4040.18799	2018-03-09	072378839	Yorke
5012525432	id_pesan23	GRAB	35636.3359	2019-02-01	042520519	Delphinia
6831075152	id_pesan24	JNE	29411.0742	2018-01-29	362359843	Esteban
3382903462	id_pesan25	GRAB	29221.457	2018-02-03	594044471	Brynne
8916887775	id_pesan26	JNE	7510.10693	2018-05-12	208035618	Delcina
5469480667	id_pesan27	GOJEK	43307.918	2018-09-11	348267153	Hedvig
6149629751	id_pesan28	JNE	42744.1953	2018-02-03	227991097	Ricky
9880654543	id_pesan29	OJEK	1126.07397	2018-06-22	495124166	Obidiah
1020250490	id_pesan30	OJEK	5844.25391	2019-03-30	646745307	Sissie
6595966046	id_pesan31	GRAB	12992.0498	2018-02-07	273118631	Lucia
8403126622	id_pesan32	JNE	23423.1758	2018-08-03	879223800	Trula
8827401220	id_pesan33	GRAB	48095.5664	2019-03-30	113631010	Ysabel
3928582564	id_pesan34	GOJEK	27136.1602	2018-06-28	199675243	Shana
4708636216	id_pesan35	GRAB	41880.6641	2018-04-25	708591408	Rafaellle
8491406339	id_pesan36	JNE	31010.1816	2018-12-09	626460056	Prissie
2904061705	id_pesan37	OJEK	35906.2383	2018-09-30	758485034	Terry
7263872987	id_pesan38	JNE	29051.5215	2018-09-09	563763171	Junette
8463127017	id_pesan39	GRAB	10861.7314	2018-08-31	630567791	Bianca
8402740543	id_pesan40	GRAB	5044.10693	2018-07-12	360665587	Rodrick
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toys_rent; Owner: db2018027
--

COPY toys_rent.status (nama, deskripsi) FROM stdin;
Status1	Barang baru sampai di Kantor TOYS RENT
Status2	Kode terdaftar
Status3	Dalam proses
Status4	Dalam proses transit
Status5	Barang ditolak
Status6	Barang diterima
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: anggota cek_update_level_trigger_anggota; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER cek_update_level_trigger_anggota AFTER INSERT OR UPDATE OF poin ON toys_rent.anggota FOR EACH ROW EXECUTE PROCEDURE toys_rent.cek_update_level();


--
-- Name: level_keanggotaan cek_update_level_trigger_level_keanggotaan; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER cek_update_level_trigger_level_keanggotaan AFTER INSERT OR DELETE OR UPDATE ON toys_rent.level_keanggotaan FOR EACH STATEMENT EXECUTE PROCEDURE toys_rent.cek_update_level_semua_anggota();


--
-- Name: pemesanan delete_poin_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER delete_poin_trigger AFTER DELETE ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_poin();


--
-- Name: pemesanan insert_update_poin_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER insert_update_poin_trigger BEFORE INSERT OR UPDATE ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_poin();


--
-- Name: barang_pesanan set_kondisi_barang; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER set_kondisi_barang AFTER INSERT ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.set_kondisi_barang();


--
-- Name: barang_dikembalikan set_kondisi_barang_pengembalian; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER set_kondisi_barang_pengembalian AFTER INSERT ON toys_rent.barang_dikembalikan FOR EACH ROW EXECUTE PROCEDURE toys_rent.set_kondisi_barang_pengembalian();


--
-- Name: barang_pesanan update_harga_sewa_pemesanan_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER update_harga_sewa_pemesanan_trigger AFTER INSERT ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_harga_sewa_pemesanan();


--
-- Name: pengiriman update_ongkos_pemesanan_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018027
--

CREATE TRIGGER update_ongkos_pemesanan_trigger AFTER INSERT ON toys_rent.pengiriman FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_ongkos_pemesanan();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp);


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES toys_rent.level_keanggotaan(nama_level);


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengembalian(no_resi);


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi);


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan);


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama);


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES toys_rent.admin(no_ktp);


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES toys_rent.level_keanggotaan(nama_level);


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES toys_rent.kategori(nama);


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES toys_rent.kategori(nama);


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama);


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama);


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi);


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan);


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018027
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

